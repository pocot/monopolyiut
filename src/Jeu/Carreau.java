package Jeu;

/**
 * Classe abstraite représentant <b>toutes</b> les cases du plateau de jeu
 * <p>
 * Un carreau est constitué:
 * <ul>
 * <li> Un numéro unique entre 1 et 40</li>
 * <li> Un nom de Carreau </li>
 * <li> Un monopoly </li>
 *
 * </ul>
 * </p>
 *
 * @see CarreauAction
 * @see CarreauMouvement
 * @see CarreauArgent
 * @see CarreauPropriete
 * @see CarreauTirage
 */

import java.io.Serializable;
public abstract class Carreau implements Serializable {

    /**
     * Numero du carreau. Non modifiable.
     *
     * @see Carreau#getNumero()
     */
    private int numero;
    /**
     * Nom du carreau.
     *
     * @see Carreau#getNomCarreau()
     * @see Carreau#setNomCarreau(java.lang.String)
     */

    private String nomCarreau;
    /**
     * La partie en cours
     *
     * @see Monopoly
     *
     */
    private Monopoly monopoly;

    /**
     * Retourne le "Monopoly" représentant la partie en cours.
     *
     * @return Monopoly.
     * @see Monopoly
     */
    public Monopoly getMonopoly() {
        return monopoly;
    }


    /**
     * Constructeur Carreau.
     *
     * @param numero Numero du carreau.
     * @param nomCarreau Nom du carreau.
     * @param monopoly La partie en cours.
     *
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     */
    public Carreau(int numero, String nomCarreau, Monopoly monopoly) {
        this.numero = numero;
        this.nomCarreau = nomCarreau;
        this.monopoly = monopoly;
    }
    
    public abstract void Action(Joueur j) throws FailliteException;

    /**
     * Renvoie le numéro du Carreau.
     *
     * @return numéro du Carreau.
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Met à jour le numéro du Carreau.
     *
     * @param numero Le nouveau numéro du Carreau.
     * @see Carreau#numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Renvoie le nom du Carreau.
     *
     * @return Nom du Carreau.
     * @see Carreau#nomCarreau
     */

    public String getNomCarreau() {
        return nomCarreau;
    }

    /**
     * Met à jour le nom du Carreau.
     *
     * @param nomCarreau Le nouveau nom du Carreau.
     * @see Carreau#nomCarreau
     */
    public void setNomCarreau(String nomCarreau) {
        this.nomCarreau = nomCarreau;
    }

}
