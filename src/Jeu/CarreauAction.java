package Jeu;

/**
 * <b>CarreauAction est la classe abstraite représentant une case d'action.</b>
 * <p>
 * Un CarreauAction est caractérisé par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de Carreau. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe Carreau.</i>
 * </p>
 *
 * @see Carreau
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 *
 */
public abstract class CarreauAction extends Carreau {

    /**
     * Contructeur CarreauAction.
     * <p>
     * Utilise le constructeur de Carreau
     * </p>
     *
     * @param numero Le numéro du Carreau.
     * @param nomCarreau Le nom du Carreau.
     * @param monopoly La partie en cours.
     * @see Carreau#Carreau(int, java.lang.String, Jeu.Monopoly)
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     */

    public CarreauAction(int numero, String nomCarreau, Monopoly monopoly) {
        super(numero, nomCarreau, monopoly);
    }
    
    

}
