package Jeu;

/**
 * <b> CareauArgent est la classe abstraite représentant un Carreau engendrant
 * une incidence sur l'argent d'un et de plusieurs joueurs</b>
 * <p>
 * Un CarreauArgent est constitué :
 * <ul>
 * <li> D'un montant </li>
 * <li> D'un numéro de Carreau. </li>
 * <li> D'un nom de Carreau. </li>
 * <li> D'une partie en cours (Monopoly).</li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe CarreauAction.</i>
 * </p>
 *
 * @see CarreauAction
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 */
public class CarreauArgent extends CarreauAction {

    /**
     * Montant d'argent mis en jeu par la case.
     */
    private int montant;

    /**
     * Constructeur CarreauArgent.
     * <p>
     * Utilise le constructeur de CarreauAction.
     * </p>
     *
     * @param montant Le montant d'argent de la case.
     * @param numero Le numéro du CarreauArgent.
     * @param nomCarreau Le nom du CarreauArgent.
     * @param monopoly La partie en cours.
     * @see #montant
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     */

    public CarreauArgent(int montant, int numero, String nomCarreau, Monopoly monopoly) {
        super(numero, nomCarreau, monopoly);
        this.montant = montant;
    }
    @Override
    public void Action(Joueur j) throws FailliteException {
        if(!(j.getPositionCourante().getNumero() == 1)){
        this.getMonopoly().interfaceTexte.casePayer(this.getNomCarreau(),j.getNomJoueur(), this.montant);
        j.debite(montant);
        }
        
        
        //TOUT DOUX : Argent à recevoir sur le parc Gratuit.  
    }
}
