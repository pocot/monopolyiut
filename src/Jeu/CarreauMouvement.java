package Jeu;

/**
 * <b> CareauMouvement est la classe représentant un Carreau engendrant un
 * mouvement d'un ou plusieurs joueurs</b>
 * <p>
 * Un CarreauMouvement est constitué :
 * <ul>
 * <li> D'un numéro de Carreau. </li>
 * <li> D'un nom de Carreau. </li>
 * <li> D'une partie en cours (Monopoly).</li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe CarreauAction.</i>
 * </p>
 *
 * @see CarreauAction
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 * *
 */
public class CarreauMouvement extends CarreauAction {

    /**
     * Constructeur de CarreauMouvement.
     *
     * @param numero Le numéro du CarreauMouvement.
     * @param nomCarreau Le nom du CarreauMouvement.
     * @param monopoly La partie en cours.
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     */
    public CarreauMouvement(int numero, String nomCarreau, Monopoly monopoly) {
        super(numero, nomCarreau, monopoly);
    }
    
    @Override
    public void Action(Joueur j) {
        j.teleportation(11);
        j.modifierPrisonnier(true);
        j.modifierRejouer(false);
    }

}
