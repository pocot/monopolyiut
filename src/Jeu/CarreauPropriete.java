package Jeu;

/**
 * <b>CarreauPropriete est la classe abstraite représentant une case achetable
 * par un joueur.</b>
 * <p>
 * Un CarreauPropriete est caractérisé par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de Carreau. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe Carreau.</i>
 * </p>
 *
 * @see Carreau
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 *
 */
public abstract class CarreauPropriete extends Carreau {

    /**
     * Le prix d'achat de la propriété.
     */
    private final int prixAchatPropriete;
    /**
     * Le loyer de la propriété une fois celle-ci achetée. Celui-ci varie selon
     * la nature du terrain.
     */
    private int loyer;
    /**
     * Le joueur possèdant la propriété. Ce joueur peut changer durant la
     * partie.
     */
    private Joueur proprietaire;

    /**
     *
     * Méthode abstraite renvoyant le loyer de la propriété.
     *
     * @param jProprio Le joueur propriétaire de cette propriété.
     * @param j Le joueur tombé sur cette propriété.
     * @return loyer de la propriété.
     * @see Joueur
     */
    public abstract int getLoyer(Joueur jProprio, Joueur j);

    /**
     * Constructeur de CarreauPropriete.
     *
     * @param numero Le numéro du CarreauPropriete.
     * @param nomCarreau Le nom du CarreauPropriete.
     * @param monopoly La partie en cours.
     * @param prixAchatPropriete Le prix d'achat de la propriété.
     *
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     * @see #prixAchatPropriete
     */
//  Il faut initialiser les attributs de la classe.
    public CarreauPropriete(int numero, String nomCarreau, Monopoly monopoly, int prixAchatPropriete) {
        super(numero, nomCarreau, monopoly);
        this.prixAchatPropriete = prixAchatPropriete;
        proprietaire = null;
        this.loyer = 0;     //getLoyer();
    }

    /**
     * Retourne le prix d'achat de la propriété.
     *
     * @return prix d'achat de la propriété.
     * @see #prixAchatPropriete
     */
    public int getPrixAchatPropriete() {
        return prixAchatPropriete;
    }

    /**
     * Met à jour le propriétaire de la propriété.
     *
     * @param j Le nouveau joueur propriétaire
     * @see Joueur
     */
    private void setProprietaire(Joueur j) {
        this.proprietaire = j;
    }
    public void nouveauProprietaire(Joueur j){
        this.setProprietaire(j);
    }
    
    public void resetProprieteaire(Joueur j) {
        this.proprietaire = null;
    }

    /**
     * Retourne le propriétaire actuel de la propriété.
     *
     * @return Le joueur propriétaire.
     * @see Joueur
     */
    public Joueur getProprietaire() {
        return proprietaire;
    }

    @Override
    public void Action(Joueur j) throws FailliteException {
        int l;
        Joueur jproprio = this.getProprietaire();
        if (jproprio == null) {
            this.achatPropriete(j);
        } else {
            if (j != jproprio) {

                l = getLoyer(jproprio, j);
                this.getMonopoly().getInterfaceTexte().payerLoyer(this.getNomCarreau(), j.getNomJoueur(), jproprio.getNomJoueur(), l);
                jproprio.credite(l);
                j.debite(l);

            }
        }
    }

    /**
     * Permet au joueur s'il a assez d'argent d'acheter cette propriété.
     *
     * @param j Joueur tombé sur cette case.
     * @see Joueur
     */
    public void achatPropriete(Joueur j) {
        int cash;
        int prix;
        cash = j.getCash();
        prix = this.getPrixAchatPropriete();
        if (cash > prix) {
            boolean rep = this.getMonopoly().getInterfaceTexte().descriptionPropriete(this);
            if (rep) {
                j.ajouterPropriete(this);
                this.setProprietaire(j);
                try {
                    j.debite(prix);
                } catch (FailliteException f) {

                }
            }

        }
        else{
            this.getMonopoly().interfaceTexte.afficheManqueArgentProp(j, this.prixAchatPropriete);
        }
    }

}
