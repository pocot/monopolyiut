package Jeu;
/**
 * <b>CarreauTirage est la classe représentant une case engendrant un tirage.</b>
 * <p>
 * Un CarreauTirage est caractérisé par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de Carreau. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * </ul>
 * </p>
 * <p>
 *  <i>Cette classe hérite de la classe CarreauAction.</i>
 * </p>
 * @see Carreau
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 * 
 */
public class CarreauTirage extends CarreauAction {
    
    /**
     * Constructeur de CarreauTirage.
     * @param numero
     *          Le numéro de la case CarreauTirage.
     * @param nomCarreau
     *          Le nom de la case CarreauTirage.
     * @param monopoly
     *          La partie en cours.
     */

     public CarreauTirage(int numero, String nomCarreau, Monopoly monopoly) {
        super(numero, nomCarreau, monopoly);
    }
     @Override
    public void Action(Joueur j) throws FailliteException {
        if(this.getNomCarreau().equals("Chance")){
            Carte temp =this.getMonopoly().tirerCarte(TypeCarte.Chance);
            temp.Action(j);
            this.getMonopoly().getInterfaceTexte().afficheCarte(temp.getType(),temp.getDescription());
        }else{
           Carte temp =this.getMonopoly().tirerCarte(TypeCarte.Communaute);
            temp.Action(j);
            this.getMonopoly().getInterfaceTexte().afficheCarte(temp.getType(),temp.getDescription());
        }
        
    }

}
