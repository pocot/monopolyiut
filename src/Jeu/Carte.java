
package Jeu;
import java.io.Serializable;

/**
 * La classe Carte est une classe abstraite regroupant toutes les cartes qu'elles soient chances ou communauté.
 * Cette classe contient
 * 
 */
public abstract class Carte implements Serializable {
    private final TypeCarte type;
    private final String description;
    private Monopoly monopoly;
    
    /**
     * Constructeur de <code>Carte</code>.
     * @param type
     *      Type de la carte.
     * @param description
     *      Texte associé à la carte.
     * @param monopoly 
     *      Partie en cours.
     */
    
    public Carte(TypeCarte type, String description,Monopoly monopoly){
        this.type = type;
        this.description = description;
        this.monopoly = monopoly;
    }
    /**
     * L'action engendrée par cette carte.
     * @param j
     *      Le joueur sur lequel s'appliquera l'action.
     * @throws FailliteException
     *      Si le joueur ne peux pas payer, il tombera en faillite.
     * @see Joueur
     * @see FailliteException
     */
    public abstract void Action(Joueur j) throws FailliteException;
    
    /**
     * Retourne la partie en cours.
     * @return 
     *      Partie en cours.
     */

    public Monopoly getMonopoly() {
        return monopoly;
    }
    
    /**
     * Retourne le type de la carte.
     * @return
     *      TypeCarte.
     * @see TypeCarte
     */
    public TypeCarte getType() {
        return type;
    }
    /**
     * Retourne la description de la carte.
     * @return 
     *      Description de la carte.
     */

    public String getDescription() {
        return description;
    }
    
    
    
    
    
    
    
}


