/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Jeu;

import java.util.ArrayList;

/**
 *
 * @author peironel
 */
public class CarteAnniversaire extends Carte {
    private final int somme;

    public CarteAnniversaire(TypeCarte type, String description, Monopoly monopoly,int somme) {
        super(type, description, monopoly);
        this.somme = somme;
    }

    
    
    
 @Override
    public void Action(Joueur j) throws FailliteException {
        
        ArrayList<Joueur> advs = j.getAdversaire(j);
        for(Joueur adv : advs){
            adv.debite(somme);
            j.credite(somme);
        }
        
    }
    
}
