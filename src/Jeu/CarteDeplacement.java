/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jeu;

/**
 *
 * @author peironel
 */
public class CarteDeplacement extends Carte {

    private final String deplacement;

    public CarteDeplacement(TypeCarte type, String description, Monopoly monopoly, String deplacement) {
        super(type, description, monopoly);
        this.deplacement = deplacement.trim();
    }

    @Override
    public void Action(Joueur j) throws FailliteException {
         int valDeplacement = Integer.parseInt(deplacement);
        if (deplacement.contains("+") || deplacement.contains("-")) {
            j.avancer(j.getPositionCourante().getNumero() + valDeplacement);
        } else {
            if (valDeplacement == 11) {
                j.modifierPrisonnier(true);
                j.modifierRejouer(false);
                j.teleportation(valDeplacement);
            } else {
                if(valDeplacement - j.getPositionCourante().getNumero() >= 0){
                    j.teleportation(valDeplacement);
                }else{
                    j.credite(200);
                    j.teleportation(valDeplacement);
                    j.getPositionCourante().Action(j);
                }
            }
        }
    }
}
