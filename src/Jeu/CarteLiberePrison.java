/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Jeu;

/**
 *
 * @author peironel
 */
public class CarteLiberePrison extends Carte{

    public CarteLiberePrison(TypeCarte type, String description, Monopoly monopoly) {
        super(type, description, monopoly);
    }
    @Override
    public void Action(Joueur j) {
        j.ajouterCarteLiberePrison();
        this.getMonopoly().ajouterCarteSortieJeu(this);
        
    }
    
}
