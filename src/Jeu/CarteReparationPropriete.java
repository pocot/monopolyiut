/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Jeu;

import java.util.ArrayList;

/**
 *
 * @author peironel
 */
public class CarteReparationPropriete extends Carte {
    private final int coutMaison;
    private final int coutHotel;

    public CarteReparationPropriete(TypeCarte type, String description, Monopoly monopoly,int coutMaison, int coutHotel) {
        super(type, description, monopoly);
        this.coutMaison = coutMaison;
        this.coutHotel = coutHotel;
    }
    
    
    
    @Override
    public void Action(Joueur j) throws FailliteException {
       ArrayList<ProprieteAConstruire> props = j.getProprietesAConstruire();
       int nbM = 0, nbH = 0;
       
       for (ProprieteAConstruire prop : props){
           nbM = nbM +prop.getNbMaisons();
           nbH = nbH + prop.getNbHotels();
       }
       j.debite(nbM*coutMaison + nbH*coutHotel);
    }
}
