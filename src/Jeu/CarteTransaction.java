/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Jeu;

/**
 *
 * @author peironel
 */
public class CarteTransaction extends Carte {
    
    private final int somme;

    public CarteTransaction(TypeCarte type, String description, Monopoly monopoly,int somme) {
        super(type, description, monopoly);
        this.somme = somme;
    }
    
    
    
    @Override
    public void Action(Joueur j) throws FailliteException{
        if(somme > 0){
            j.credite(somme);
        }else{
            j.debite(java.lang.Math.abs(somme));
        }
    
}
}
