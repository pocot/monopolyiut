package Jeu;

/**
 * <b>Compagnie est la classe représentant les case compagnies des eux et de
 * l'électricité.</b>
 * <p>
 * Une compagnie est caractérisée par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de Compagnie. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * <li> Le prix d'achat de la compagnie.
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe CarreauPropriete.</i>
 * </p>
 *
 * @see CarreauPropriete
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 */
public class Compagnie extends CarreauPropriete {

    /**
     * Constructeur de la Compagnie.
     *
     * @param numero Le numéro de la case.
     * @param nomCarreau Le nom de la compagnie.
     * @param monopoly La partie en cours.
     * @param prixAchat Le prix d'achat de la compagnie.
     *
     * @see Carreau#numero
     * @see Carreau#nomCarreau
     * @see Carreau#monopoly
     * @see CarreauPropriete#prixAchatPropriete
     */
    public Compagnie(int numero, String nomCarreau, Monopoly monopoly, int prixAchat) {
        super(numero, nomCarreau, monopoly, prixAchat);
    }

    /**
     * Retourne le loyer de la compagnie.
     *
     * @param jProprio Le joueur propriétaire de la compagnie.
     * @param j Le joueur tombé sur cette compagnie.
     * @return Le loyer de la compagnie.
     */

    @Override
    public int getLoyer(Joueur jProprio, Joueur j) {
        int[] dés = j.getDernierDés();
        if (jProprio.getCompagnies().size() == 1) {       // Le propriétaire possède 1 compagnie.
            return (dés[0] + dés[1]) * 4;
        } else {                              // Le propriétaire possède 2 compagnies.
            return (dés[0] + dés[1]) * 10;
        }
    }
}
