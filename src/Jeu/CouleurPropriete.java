package Jeu;

import java.io.Serializable;

/**
 * CouleurPropriete défini les différentes couleurs possibles pour un
 * ProprieteAConstruire.
 *
 */
public enum CouleurPropriete implements Serializable {

    bleuFonce, orange, mauve, violet, bleuCiel, jaune, vert, rouge;
}
