/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Jeu;

import Ui.text.Interface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Scénarios pour la démonstration du projet montrants toutes les
 * fonctionnalités possible à travers une partie à 2 joueurs trucée.
 *
 * @author bonnardk
 */
public class Demo {
    public static int compteurDéfaite;
    public static ArrayList<Integer> des;
    public static int compteurDés;

    private static void coupScenario(Joueur j, Monopoly m) {
        try {
            if (!j.isFaillite()) {
                m.jouerUnCoup(j);
            }
        } catch (FailliteException fe) {
        }

        System.out.println(
                "________________________________tour de scénario________________________________");
        System.out.println(
                "________________________________________________________________________________");
    }

    private static void coupScenario2(Joueur j, Monopoly m, Interface in) {
        try {
            if (!j.isFaillite()) {
                m.jouerUnCoup(j);
            }
        } catch (FailliteException fe) {
            j.enFaillite();
            in.faillite(j);
            compteurDéfaite++;
        }
        if(compteurDéfaite == m.getJoueurs().size() - 1){
            
        }

    }

    private static void tricheCarte(Monopoly m, String des, TypeCarte tc) {
        Carte c2 = null;
        if (tc == TypeCarte.Chance) {

            for (Carte c : m.getCartesChance()) {
                if (c.getDescription().contains(des)) {
                    m.MettreAuDessusPaquet(tc, c);
                }
            }
        } else {
            for (Carte ctemp : m.getCartesCommunaute()) {
                if (ctemp.getDescription().contains(des)) {
                    m.MettreAuDessusPaquet(tc, ctemp);
                    break;
                }
            }
        }
    }

//    private static void tour(Monopoly m, Joueur j, int d1, int d2) {
//        int[] dés = new int[2];
//        dés[0] = d1;
//        dés[1] = d2;
//        j.nouveauDernierDés(dés);
//        coupScenario(j, m);
//    }
    private static void prediction(String txt) {
        System.out.println();
        for (int i = 1; i <= txt.length() + 4; i++) {
            System.out.print("*");
        }
        System.out.println("\n* " + txt + " *");
        for (int i = 1; i <= txt.length() + 4; i++) {
            System.out.print("*");
        }
        System.out.println();

    }

    private static void initialisationDés(String nomF) {
        des = new ArrayList();
        compteurDés = 0;
        try {
            ArrayList<String[]> data = readDataFile(nomF, ",");

            for (int i = 0; i < data.get(0).length; i++) {
                des.add(Integer.parseInt(data.get(0)[i]));
            }
        } catch (FileNotFoundException fnfe) {

        } catch (IOException ioe) {

        }
    }

    private static ArrayList<String[]> readDataFile(String filename, String token) throws FileNotFoundException, IOException {
        ArrayList<String[]> data = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = null;
        while ((line = reader.readLine()) != null) {
            data.add(line.split(token));
        }
        reader.close();

        return data;
    }

    private static void waitContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.println("(Continuer démo...?)");
        sc.nextLine();
    }

    private static void donneProp(Monopoly m, int num, Joueur j) {
        Carreau car = m.getCarreaux().get(num);
        ProprieteAConstruire pap = (ProprieteAConstruire) car;
        j.addProprieteAConstruire(pap);
        pap.nouveauProprietaire(j);
        System.out.println(j.getNomJoueur() + " obtient " + pap.getNomCarreau());
    }

    public static void main(String[] args) {
        initialisationDés("désDemo1.txt");
        Monopoly m = new Monopoly("src/Data/data.txt", "src/Data/cartes.txt");
        Interface interfaceTexte = new Interface(m);
        m.modifierInterfaceTexte(interfaceTexte);
        m.addJoueur("Jean-Scott");
        m.addJoueur("Jean-Lucas");
        Joueur j1 = m.getJoueurs().get(0);
        Joueur j2 = m.getJoueurs().get(1);
        
        System.out.println("________________________________tour de scénario________________________________");
        System.out.println("________________________________________________________________________________");
        prediction("vous allez tomber sur une propriete à vendre, ACHETEZ LA");
        waitContinue();
        coupScenario(j1,m);
        waitContinue();
        coupScenario(j2,m);
        waitContinue();

        prediction("vous allez tomber sur une propriete à vendre, NE L'ACHETEZ PAS");
        waitContinue();
        coupScenario(j1,m); //refus achat prop
        waitContinue();
        tricheCarte(m, "Vous êtes libéré(e) de prison. Vous pouvez conserver cette carte jusqu'a son utilisation", TypeCarte.Chance);
        coupScenario(j2,m);// chance, CLP
        waitContinue();

        prediction("vous allez faire un double");
        prediction("ACHETEZ la companie et la gare");
        waitContinue();
        coupScenario(j1,m);//1 double, achat comp, achat gare
        waitContinue();
        coupScenario(j2,m);//paie la comp
        waitContinue();
        
        tricheCarte(m,"C'est votre anniversaire ! Chaque joueur vous donne 10€ !", TypeCarte.Communaute);
        coupScenario(j1,m);//1 double, caisse de commu, parc gratuit
        waitContinue();
        coupScenario(j2,m);//paie la gare
        waitContinue();
        
        tricheCarte(m,"Amende pour excés de vitesse. Payez 15€",TypeCarte.Chance);
        prediction("Achetez la deuxième gare");
        waitContinue();
        coupScenario(j1,m);//achat gare 2
        waitContinue();
        coupScenario(j2,m);//parc gratuit tempo
        waitContinue();
        
        prediction("ACHETEZ la deuxième compagnie");
        waitContinue();
        coupScenario(j1,m);//achat comp2
        waitContinue();
        coupScenario(j2,m);//paie gare x2
        waitContinue();
        
        tricheCarte(m,"Avancez jusqu'a la case départ. Recevez 200€",TypeCarte.Chance);
        coupScenario(j1,m);//chance+3 doubles+ cade 
        waitContinue();
        coupScenario(j2,m);//paie gare x2
        waitContinue();
        
        coupScenario(j1,m);//echoue au double pour sortir
        waitContinue();
        coupScenario(j2,m);//va en prison
        waitContinue();
        
        coupScenario(j1,m);//idem
        waitContinue();        
        tricheCarte(m,"Allez en prison. Ne passez pas par la case départ et ne recevez pas 200€", TypeCarte.Chance);
        prediction("N'UTILISEZ pas votre carte");
        waitContinue();
        coupScenario(j2,m);//fait un double et sort pour aller en chance --> prison again
        waitContinue();
        
        coupScenario(j1,m);//echoue et paie et va en parc gratuit
        waitContinue();
        prediction("UTILISEZ votre carte pour sortir");
        waitContinue();
        coupScenario(j2,m);//utilise sa carte et va en parc
        waitContinue();
        
        prediction("Nous allons maintenant changer");
        prediction("completement de situation...");
        waitContinue();

        initialisationDés("désDemo2.txt");
        Monopoly m2 = new Monopoly("src/Data/data.txt", "src/Data/cartes.txt");
        Interface interfaceTexte2 = new Interface(m2);
        m2.modifierInterfaceTexte(interfaceTexte2);
        m2.addJoueur("Jean-Scott");
        m2.addJoueur("Jean-Lucas");
        m2.addJoueur("Jean-Yann");
        Joueur j1bis = m2.getJoueurs().get(0);
        Joueur j2bis = m2.getJoueurs().get(1);
        Joueur j3 = m2.getJoueurs().get(2);

        donneProp(m2, 38, j1bis);
        donneProp(m2, 4, j1bis);
        donneProp(m2, 7, j1bis);

        donneProp(m2, 10, j2bis);
        donneProp(m2, 14, j2bis);
        Carreau car = m2.getCarreaux().get(16);
        Gare g = (Gare) car;
        j2bis.addGares(g);
        g.nouveauProprietaire(j2bis);
        System.out.println(j2bis.getNomJoueur() + " obtient " + g.getNomCarreau());

        donneProp(m2, 17, j3);
        donneProp(m2, 25, j3);
        Carreau car2 = m2.getCarreaux().get(29);
        Compagnie comp = (Compagnie) car2;
        j3.addCompagnie(comp);
        comp.nouveauProprietaire(j3);
        System.out.println(j3.getNomJoueur() + " obtient " + comp.getNomCarreau());

        j1bis.teleportation(36);
        j2bis.teleportation(36);
        j3.teleportation(36);

        j1bis.credite(5000);

        prediction("ACHETEZ le bleu manquant et CONSTRUISEZ des hôtels");
        waitContinue();
        coupScenario(j1bis, m2);
        waitContinue();
        coupScenario2(j2bis, m2,interfaceTexte2);//faillite
                System.out.println(
                "________________________________tour de scénario________________________________");
        System.out.println(
                "________________________________________________________________________________");
        waitContinue();
        coupScenario2(j3, m2, interfaceTexte2);//faillite
        
        System.out.println(
                "________________________________fin de scénario_________________________________");
        System.out.println(
                "________________________________________________________________________________");
                Scanner sc = new Scanner(System.in);
        System.out.println("...");
        sc.nextLine();


        }

    }

    


