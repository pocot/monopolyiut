package Jeu;

/**
 *Exception lancée en cas de faillite du joueur, cette faillite peut être déclenchée par l'abandon du joueur ou par le fait que son
 *argent atteigne la somme de 0.
 * 
 */
public class FailliteException extends Exception {

    /**
     * Constructeur de <code>FailliteException</code>.
     */
    public FailliteException() {
    }
}
