package Jeu;

/**
 * <b>Gare est une classe représentant les différentes gares du jeu.</b>
 * <p>
 * Une gare est caractérisée par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de Gare. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * <li> Le prix d'achat de la gare. </li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe CarreauPropriete.</i>
 * </p>
 *
 */
public class Gare extends CarreauPropriete {

    /**
     * Constructeur d'une Gare.
     *
     * @param numero Le numéro de la case de la gare.
     * @param nomCarreau Le nom de la gare.
     * @param monopoly La partie en cours.
     * @param prixAchat Le prix d'achat de la gare.
     */
    public Gare(int numero, String nomCarreau, Monopoly monopoly, int prixAchat) {
        super(numero, nomCarreau, monopoly, prixAchat);
    }

    /**
     * Retourne le loyer de la gare.
     *
     * @param jProprio Le joueur propriétaire de la gare.
     * @param j Le joueur tombé sur la case.
     * @return Le loyer de la gare.
     *
     * +
     */
    @Override
    public int getLoyer(Joueur jProprio, Joueur j) {
        return 25 * jProprio.getNbGares();
    }

    /*@Override
     public int calculLoyer(Joueur jproprio, Joueur j) {
     int nbGares = j.getNbGares();
     return j.getNbGares()*25;
     }*/
}
