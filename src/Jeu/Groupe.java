package Jeu;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * <b>Groupe est la classe représentant les groupe de proprieteAConstruire.</b>
 * <p>
 * Un groupe est caractérisé par :
 * <ul>
 * <li> Une couleur de propriété.</li>
 * <li> Le prix d'une construction(maison/hotel) sur une propriété du
 * groupe</li>
 * <li> Les propriétés qui le constitue.</li>
 * </ul>
 * </p>
 *
 * @see CouleurPropriete
 * @see #prixConstruction
 * @see #proprietes
 *
 *
 *
 */
public class Groupe implements Serializable{

    /**
     * La couleur du groupe.
     */
    private CouleurPropriete couleur;
    /**
     * Le prix de la construction d'une maison ou d'un hotel pour ce groupe.
     */
    private int prixConstruction;
    /**
     * Les propriétés constituants le groupe.
     */
    private ArrayList<ProprieteAConstruire> proprietes;

    /**
     * Constructeur du Groupe.
     *
     * @param couleur La couleur de ce groupe.
     * @param prixConstruction Le prix de construction associé à ce groupe.
     * @param paraProps Les paramètres permettants la construction des
     * propriétés dans le groupe.
     * @see #couleur
     * @see #prixConstruction
     *
     */
    public Groupe(CouleurPropriete couleur, int prixConstruction, ArrayList<ArrayList<Object>> paraProps){
        this.couleur = couleur;
        this.prixConstruction = prixConstruction;
        proprietes = new ArrayList<>();
        for (ArrayList<Object> ar1 : paraProps) {

            ProprieteAConstruire temp = new ProprieteAConstruire((Integer) ar1.get(0), (String) ar1.get(1), (Monopoly) ar1.get(2), (Integer) ar1.get(3), (ArrayList<Integer>) ar1.get(4), this);
            proprietes.add(temp);
            Monopoly m = (Monopoly) ar1.get(2);
            m.ajouterCarreau((Integer) ar1.get(0), temp);
        }
    }

    /**
     * Retourne le prix de construction d'une maison ou d'un hôtel.
     *
     * @return Prix de la construction.
     * @see #prixConstruction
     */

    public int getPrixConstruction() {
        return prixConstruction;
    }


    /**
     * Retourne les propriété constituant le groupe.
     *
     * @return Propriété constituant le groupe.
     */
    public ArrayList<ProprieteAConstruire> getProprietes() {
        return proprietes;
    }

    /**
     * Renvoi la couleur du groupe.
     * @return couleur du groupe
     */

    public CouleurPropriete getCouleur() {
        return couleur;
    }

    

}
