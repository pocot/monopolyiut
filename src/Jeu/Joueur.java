package Jeu;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <b>Joueur est la classe représentant un joueur humain du jeu.</b>
 * <p>
 * Un joueur est caractérisé par
 * <ul>
 * <li> Un nom de joueur. </li>
 * <li> La partie en cours. </li>
 * </ul>
 * </p>
 *
 * @see Monopoly
 *
 */
public class Joueur implements Serializable {

    /**
     * Le nom du joueur.
     */
    private String nomJoueur;
    /**
     * L'argent du joueur. Il est fixé au départ à 1500€.
     */
    private int cash;
    /**
     * La partie en cours.
     *
     * @see Monopoly
     */
    private Monopoly monopoly;
    /**
     * Les compagnies possédées par ce joueur.
     */
    private ArrayList<Compagnie> compagnies;
    /**
     * Les gares possédées par ce joueur.
     */
    private ArrayList<Gare> gares;
    /**
     * La position du joueur sur le plateau.
     */
    private Carreau positionCourante;
    /**
     * Les propriétés possédées par ce joueur.
     */
    private ArrayList<ProprieteAConstruire> proprietesAConstruire;
    /**
     * <p>
     * Défini l'état de liberté du joueur.</br>
     * True = Prisonnier</br>
     * False = Libre.</br>
     * </p>
     * <p>
     * Cet état est par défaut à faux.</p>
     */
    private Boolean prisonnier;
    /**
     * <p>
     * Défini si le joueur à la possibilité de rejouer ou non.</br>
     * True = Peut Rejouer</br>
     * False = Ne peut pas rejouer </br>
     * </p>
     * <p>
     * Cet état est par défaut à faux </p>
     */
    private Boolean rejouer;
    /**
     * Le nombre de double d'affilé fait par un joueur.
     */
    private int nbDouble;
    /**
     * Le nombre de tour passé en prison par le joueur à partir du momment où il
     * y est rentré.
     */
    private int nbTourPrison;

    /**
     * Les résultats du dernier lancé de dés de ce joueur.
     */
    private int[] dernierDés;

    /**
     * Si le joueur a fait ou non fallite.
     */
    private boolean faillite;
    
    /**
     * Nombre de carte libérer de prison que le joueur possède.
     * @see #prisonnier
     */
    private int nbCarteLiberePrison;

    /**
     * Constructeur d'un Joueur.
     *
     * @param nomJoueur Le nom du joueur.
     * @param monopoly La partie en cours.
     * @see Monopoly
     */
    public Joueur(String nomJoueur, Monopoly monopoly) {
        this.nomJoueur = nomJoueur;
        this.monopoly = monopoly;
        compagnies = new ArrayList<>();
        gares = new ArrayList<>();
        proprietesAConstruire = new ArrayList<>();
        nbDouble = 0;
        rejouer = false;
        prisonnier = false;
        nbTourPrison = 0;
        positionCourante = monopoly.getCarreaux().get(1);
        faillite = false;
        cash = 1500;
    }
    
    public void enFaillite(){
        for(ProprieteAConstruire p : this.getProprietesAConstruire()){
            p.resetProprieteaire(this);
            this.getMonopoly().ajouterHotels(p.getNbHotels());
            this.getMonopoly().ajouterMaisons(p.getNbMaisons());
            p.modifierNbHotels(0);
            p.modifierNbMaisons(0);
            
        }
        for(Gare g : this.getGares()){
            g.resetProprieteaire(this);
        }
        for(Compagnie c :this.getCompagnies()){
            c.resetProprieteaire(this);
        }
        this.setFaillite(true);
    }

    /**
     * Retourne les valeurs des dés du dernier lancer de ce joueur.
     *
     * @return Valeur des dés du dernier lancer.
     */

    public int[] getDernierDés() {
        return dernierDés;
    }

    /**
     * Met à jour la valeur des derniers dés lancés par ce joueur.
     *
     * @param dernierDés Les valeurs des deux dés.
     */

    private void setDernierDés(int[] dernierDés) {
        this.dernierDés = dernierDés;
    }
    
    public void nouveauDernierDés(int[] dernierDés) {
        this.setDernierDés(dernierDés);
    }

    /**
     * Retourne la partie en cours.
     *
     * @return La partie en cours.
     */
    public Monopoly getMonopoly() {
        return monopoly;
    }

    /**
     * Augmente l'argent du joueur du montant en paramètre.
     *
     * @param l somme.
     */
    public void credite(int montant) {
        this.setCash(this.getCash() + montant);
    }

    /**
     * Retourne le nombre de gares du joueur.
     *
     * @return Le nombre de gares du joueur.
     */
    public int getNbGares() {
        return this.getGares().size();
    }

    /**
     * Met à jour l'argent du joueur.
     *
     * @param cash Le nouveau montant d'argent.
     * @see #cash
     */
    private void setCash(int cash) {
        this.cash = cash;
    }

    /**
     * Retourne l'argent du joueur.
     *
     * @return L'argent du joueur.
     * @see #cash
     */
    public int getCash() {
        return this.cash;
    }

    /**
     * Ajoute une gare à celles possédées par le joueur.
     *
     * @param g Gare à ajouter.
     * @see #gares
     */
    public void addGares(Gare g) {
        this.gares.add(g);
    }

    /**
     * Ajoute une compagnie à celles possédées par le joueur.
     *
     * @param c Compagnie à ajouter.
     * @see #compagnies
     */
    public void addCompagnie(Compagnie c) {
        this.compagnies.add(c);
    }

    /**
     * Ajoute une proprieteAConstruire au joueur.
     *
     * @param pap Propriété à construire à ajouter.
     * @see #proprietesAConstruire
     */
    public void addProprieteAConstruire(ProprieteAConstruire pap) {
        this.proprietesAConstruire.add(pap);
    }

    /**
     * Enlève au joueur un montant d'argent.
     *
     * @param prix Argent à enlever.
     * @throws FailliteException Si le joueur ne peut pas payer, l'exception est
     * levée.
     */
    public void debite(int prix) throws FailliteException {
        if (prix < this.getCash()) {
            this.setCash(this.getCash() - prix);
        } else {
            throw new FailliteException();
        }
    }


    /**
     * Augmente d'un le nombre de double d'affilé du joueur.
     *
     * @see #nbDouble
     */
    public void addNbDouble() {
        this.nbDouble++;
    }

    /**
     * Fait avancer le joueur sur le plateau du montant des deux dés.
     *
     * @param d1 Montant du premier dés.
     * @param d2 Montant du second dés.
     */
    public void avancer(int d1, int d2) {
        int placeEstimée = this.getPositionCourante().getNumero() + d1 + d2;
        if (placeEstimée > 40) {
            this.setPositionCourante(monopoly.getCarreaux().get(placeEstimée - 40));
            this.setCash(this.getCash() + 200);
        } else {
            this.setPositionCourante(monopoly.getCarreaux().get(this.getPositionCourante().getNumero() + d1 + d2));
        }

    }
    /**
     * Fait avancer le joueur sur le plateau de la valeur mise en paramètre.
     *
     * @param depl
     *      Montant du déplacement.
     */
        public void avancer(int depl) {
        int placeEstimée = this.getPositionCourante().getNumero() + depl;
        if (placeEstimée > 40) {
            this.setPositionCourante(monopoly.getCarreaux().get(placeEstimée - 40));
            this.setCash(this.getCash() + 200);
        } else {
            this.setPositionCourante(monopoly.getCarreaux().get(this.getPositionCourante().getNumero() + depl));
        }

    }
    

    /**
     * Retourne le nombre de double d'affilé du joueur.
     *
     * @return Nombre de double.
     */
    public int getNbDouble() {
        return nbDouble;
    }

    /**
     * Met à jour le nombre de double d'affilé du joueur.
     *
     * @param nbDouble Le nouveau nombre de double.
     */
    private void setNbDouble(int nbDouble) {
        this.nbDouble = nbDouble;
    }

    /**
     * Remet le nombre de double d'affilé du joueur à 0.
     */
    public void resetNbDouble() {
        this.setNbDouble(0);
    }

    /**
     * Retourne le nom de ce joueur.
     *
     * @return Nom du joueur.
     */
    public String getNomJoueur() {
        return nomJoueur;
    }

    /**
     * Retourne les compagnies possédés par le joueur.
     *
     * @return Collection de Compagnie.
     */
    public ArrayList<Compagnie> getCompagnies() {
        return compagnies;
    }



    /**
     * Retourne les gares du joueur.
     *
     * @return ArrayList de Gare.
     */
    public ArrayList<Gare> getGares() {
        return gares;
    }



    /**
     * Retourne la position actuelle du joueur sur le plateau.
     *
     * @return Carreau.
     */
    public Carreau getPositionCourante() {
        return positionCourante;
    }

    /**
     * Met à jour la position actuelle du joueur.
     *
     * @param positionCourante Position du joueur.
     */
    private void setPositionCourante(Carreau positionCourante) {
        this.positionCourante = positionCourante;
    }
    
    public void modifierPositionCourante(Carreau positionCourante){
        this.setPositionCourante(positionCourante);
    }

    /**
     * Retourne les proprieteAConstruire du joueur.
     *
     * @return ArrayList ProprieteAConstruire.
     */
    public ArrayList<ProprieteAConstruire> getProprietesAConstruire() {
        return proprietesAConstruire;
    }


    /**
     * Met à jour le statut de liberté du joueur.
     *
     * @param prisonnier Statut booléen.
     * @see #prisonnier
     */
    private void setPrisonnier(Boolean prisonnier) {
        this.prisonnier = prisonnier;
    }
    
    public void modifierPrisonnier(Boolean prisonnier){
        this.setPrisonnier(prisonnier);
    }

    /**
     * Met à jour la possibilté de rejouer du joueur.
     *
     * @param rejouer Statut booléen.
     * @see #rejouer
     */
    private void setRejouer(Boolean rejouer) {
        this.rejouer = rejouer;
    }
    
    public void modifierRejouer(Boolean rejouer){
        this.setRejouer(rejouer);
    }

    /**
     * Renvoie le statut de liberté du joueur.
     *
     * @return Le statut booléen.
     */
    public Boolean getPrisonnier() {
        return prisonnier;
    }

    /**
     * Renvoie la possibilité de rejouer du joueur.
     *
     * @return Le statut booléen.
     */
    public Boolean getRejouer() {
        return rejouer;
    }

    /**
     * Renvoie le nombre de tour qu'un joueur à passé en prison.
     *
     * @return Nombre de tours.
     * @see #nbTourPrison
     */
    public int getNbTourPrison() {
        return nbTourPrison;
    }

    /**
     * Met à jour le nombre de tour qu'un joueur à passé en prison.
     *
     * @param nbTourPrison Nombre de tours.
     * @see #nbTourPrison
     */
    private void setNbTourPrison(int nbTourPrison) {
        this.nbTourPrison = nbTourPrison;
    }

    /**
     * Ajoute un tour passé en prison par le joueur.
     *
     * @see #nbTourPrison
     */
    public void addNbTourPrison() {
        this.setNbTourPrison(this.getNbTourPrison() + 1);
    }

    /**
     * Remet le nombre de tour passé en prison par le joueur à 0.
     *
     * @see #nbTourPrison
     */
    public void resetNbTourPrison() {
        this.setNbTourPrison(0);
    }

    /**
     * Ajoute une propriété(Gare,ProprieteAConstruire ou Compagnie) au joueur.
     *
     * @param cp La propriété à ajouter.
     */
    public void ajouterPropriete(CarreauPropriete cp) {
        String className = cp.getClass().getSimpleName();
        if (className.equals("Gare")) {
            Gare temp = (Gare) cp;
            this.addGares(temp);
        } else if (className.equals("Compagnie")) {
            Compagnie temp = (Compagnie) cp;
            this.addCompagnie(temp);

        } else {
            ProprieteAConstruire temp = (ProprieteAConstruire) cp;
            this.addProprieteAConstruire(temp);
        }
    }

    /**
     * Renvoi un booléen représentant si le joueur possède ou non toutes les
     * propriété constituant le groupe de la propriété mise en paramètre.
     *
     * @param p La propriété dont la possession du groupe est à vérifier.
     * @return Statut booléen.
     */
    public boolean hasFullGroupe(ProprieteAConstruire p) {
        return this.getProprietesAConstruire().containsAll(p.getGroupePropriete().getProprietes());
    }

    //Ici il faut regarder si le joueur possède toutes les proprietes du goupe de la propriete où il est.
    /**
     * Renvoi l'état de faillite ou non du joueur.
     *
     * @return True = faillite; False = Peut jouer.
     */
    public boolean isFaillite() {
        return faillite;
    }

    /**
     * Met à jour l'état de faillite du joueur.
     *
     * @param faillite True = faillite False = Peut jouer.
     */

    private void setFaillite(boolean faillite) {
        this.faillite = faillite;
    }
    
    /**
     * Met à jour la position de ce joueur en le "téléportant".
     * Il ne passera donc pas par la case Départ.
     * @param numCarreau
     *          Numero du carreau sur lequel téléporter ce joueur.
     */
    
    public void teleportation(int numCarreau){
        this.setPositionCourante(this.monopoly.getCarreaux().get(numCarreau));
    }
    
    /**
     * Retourne les joueurs autre que celui en paramètre.
     * @param j
     *      Joueur actuel.
     * @return 
     *      Collection adversaire.
     */
    
    public ArrayList<Joueur> getAdversaire(Joueur j){
        ArrayList<Joueur> temp = new ArrayList();
        for(Joueur adv :this.monopoly.getJoueurs()){
            if(!adv.equals(j)){
                temp.add(adv);
            }
        }
        return temp;
    }
    /**
     * Retourne le nombre de carte "libéré de prison de ce joueur"
     * @return
     *      Nombre de carte.
     */

    public int getNbCarteLiberePrison() {
        return nbCarteLiberePrison;
    }
    /**
     * Met à jour le nombre de carte "libéré de prison" de ce joueur.
     * @param nbCarteLiberePrison 
     */

    private void setNbCarteLiberePrison(int nbCarteLiberePrison) {
        this.nbCarteLiberePrison = nbCarteLiberePrison;
    }
    /**
     * Ajoute une carte "libéré de prison" à ce joueur.
     */
    public void ajouterCarteLiberePrison(){
        this.setNbCarteLiberePrison(this.getNbCarteLiberePrison() +1);
    }
    /**
     * Enlève une carte "libéré de prison" au joueur puis remet cette carte dans un paquet.
     */
    public void enleverCarteLiberePrison(){
        this.setNbCarteLiberePrison(this.getNbCarteLiberePrison() -1);
        this.getMonopoly().remettreCarteJeu();
    }

}
