package Jeu;

import Ui.text.Interface;
import java.io.IOException;

/**
 * Classe principale en cas de déroulement normal d'une partie.
 * 
 */

public class Main {

    public static void main(String[] args) {
        

        Monopoly m = new Monopoly("src/Data/data.txt","src/Data/cartes.txt"); //Creation instance de Monopoly.
        Interface interfaceTexte = new Interface(m);
        m.modifierInterfaceTexte(interfaceTexte);
        String nomFichier = null;
        
        
        boolean ok = false;                             //Charger ou démarrer une partie
        do{
         nomFichier = interfaceTexte.ChargerouRedemarrer();
        if(nomFichier == "-1"){ok = true;}
        if(nomFichier != null && ok == false){
            try{
            m = m.chargerPartie(nomFichier);
            ok = true;
            }catch(IOException ioe){
                System.out.println("Fichier non trouvé");
            }
        }}
        while(!ok);
          
        
        
        if(nomFichier == "-1"){                         // Si nouvelle partie, affichage méthodes correspondantes.
        interfaceTexte.menuDebutJeu();
        interfaceTexte.determinerQuiCommence();
        }
        
        
        int compteurDéfaite = 0;                        //Boucles de jeu.
        while (compteurDéfaite != m.getJoueurs().size() - 1) {
            for (Joueur j : m.getJoueurs() ) {
                if(compteurDéfaite == m.getJoueurs().size() - 1){
                       break;
                }
                if (!j.isFaillite()) {
                    try {
                        m.jouerUnCoup(j);
                    } catch (FailliteException f) {
                        j.enFaillite();
                        interfaceTexte.faillite(j);
                        compteurDéfaite++;
                    }
                }
            
            }
        }

    }
}
