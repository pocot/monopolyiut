package Jeu;

import Ui.text.Interface;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Random;

/**
 * La classe Monopoly représente la partie en cours. Cette partie est constitué
 * des carreaux constitiuant le plateau.
 */
public class Monopoly implements Serializable {

    /**
     * Le nombre de maison disponibles dans la partie.
     */
    private int nbMaisons = 32;
    /**
     * Le nombre d'hôtel disponibles dans la partie.
     */
    private int nbHotels = 12;
    /**
     * Représentant les carreaux du plateau.
     */
    private HashMap<Integer, Carreau> carreaux;
    /**
     * Les joueurs de la partie.
     *
     * @see Joueur
     */
    private ArrayList<Joueur> joueurs;
    /**
     * L'interface à utiliser pour la partie.
     *
     * @see Interface
     */
    public Interface interfaceTexte;
    /**
     * Les cartes chances.
     */

    private LinkedList<Carte> cartesChance;
    /**
     * Les cartes communauté.
     */
    private LinkedList<Carte> cartesCommunaute;
    /**
     * Collection "mémoire" des cartes libérés de prison possédées par les
     * joueurs.
     *
     * @see CarteLiberePrison
     * @see Joueur#enleverCarteLiberePrison()
     */
    private LinkedList<CarteLiberePrison> carteSortiesJeu;

    /**
     * Constructeur du Monopoly.
     *
     * @param dataFilename Chemin du fichier contenant les données carreaux du
     * plateau.
     * @param cardFile Chemin du fichier contenant les données des cartes chance
     * et communauté.
     * @see #buildGamePlateau(java.lang.String)
     */
    public Monopoly(String dataFilename, String cardFile) {
        carreaux = new HashMap<Integer, Carreau>();
        buildGamePlateau(dataFilename);
        joueurs = new ArrayList();
        cartesChance = new LinkedList();
        cartesCommunaute = new LinkedList();
        buildCarte(cardFile);
        this.melangerCartes(cartesChance);
        this.melangerCartes(cartesCommunaute);
        carteSortiesJeu = new LinkedList();

    }

    /**
     * Construit les cartes chances et communauté.
     *
     * @param dataFileName Chemin du fichier des données des cartes.
     */
    public void buildCarte(String dataFileName) {
        try {
            ArrayList<String[]> data = readDataFile(dataFileName, ",");
            for (int i = 0; i < data.size(); ++i) {
                String caseType = data.get(i)[0];
                if (caseType.compareTo("CA") == 0) {
                    CarteAnniversaire temp = new CarteAnniversaire(TypeCarte.valueOf(data.get(i)[1]), data.get(i)[2], this, Integer.parseInt(data.get(i)[3]));
                    if (temp.getType() == TypeCarte.Chance) {
                        cartesChance.add(temp);
                    } else {
                        cartesCommunaute.add(temp);
                    }
                } else if (caseType.compareTo("CD") == 0) {
                    CarteDeplacement temp = new CarteDeplacement(TypeCarte.valueOf(data.get(i)[1]), data.get(i)[2], this, data.get(i)[3]);
                    if (temp.getType() == TypeCarte.Chance) {
                        cartesChance.add(temp);
                    } else {
                        cartesCommunaute.add(temp);
                    }
                } else if (caseType.compareTo("CLP") == 0) {
                    CarteLiberePrison temp = new CarteLiberePrison(TypeCarte.valueOf(data.get(i)[1]), data.get(i)[2], this);
                    if (temp.getType() == TypeCarte.Chance) {
                        cartesChance.add(temp);
                    } else {
                        cartesCommunaute.add(temp);
                    }
                } else if (caseType.compareTo("CRP") == 0) {
                    CarteReparationPropriete temp = new CarteReparationPropriete(TypeCarte.valueOf(data.get(i)[1]), data.get(i)[2], this, Integer.parseInt(data.get(i)[3]), Integer.parseInt(data.get(i)[4]));
                    if (temp.getType() == TypeCarte.Chance) {
                        cartesChance.add(temp);
                    } else {
                        cartesCommunaute.add(temp);
                    }
                } else if (caseType.compareTo("CT") == 0) {
                    CarteTransaction temp = new CarteTransaction(TypeCarte.valueOf(data.get(i)[1]), data.get(i)[2], this, Integer.parseInt(data.get(i)[3]));
                    if (temp.getType() == TypeCarte.Chance) {
                        cartesChance.add(temp);
                    } else {
                        cartesCommunaute.add(temp);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("[buildGamePlateau()] : File is not found!");
        } catch (IOException e) {
            System.err.println("[buildGamePlateau()] : Error while reading file!");
        }
    }

    /**
     * Construit le plateau de jeu en construisant les plateaux.
     *
     * @param dataFilename Chemin du fichier contenant les données du plateau.
     */
    private void buildGamePlateau(String dataFilename) {
        ArrayList<ArrayList<Object>> paraProps = new ArrayList<>();
        String savCouleur = null;
        boolean recommencer = false;
        int savPrixConstruction = 0;

        try {
            ArrayList<String[]> data = readDataFile(dataFilename, ",");

            for (int i = 0; i < data.size(); ++i) {
                String caseType = data.get(i)[0];
                if (caseType.compareTo("P") == 0) {

                    do {
                        recommencer = false;
                        if (savCouleur == null) {
                            savCouleur = data.get(i)[3];
                            savPrixConstruction = Integer.parseInt(data.get(i)[12]);
                            recommencer = true;
                        } else {
                            if (data.get(i)[3].equals(savCouleur)) {
                                ArrayList<Object> temp = new ArrayList<>();
                                temp.clear();
                                temp.add(Integer.parseInt(data.get(i)[1]));
                                temp.add(data.get(i)[2]);
                                temp.add(this);
                                temp.add(Integer.parseInt(data.get(i)[4]));
                                ArrayList<Integer> tablePrix = new ArrayList<Integer>();
                                int j = 5;
                                while (j < 11) {
                                    //for (int j = 5; j == 11; j++) {
                                    //System.out.println(Integer.parseInt(data.get(i)[j]));
                                    tablePrix.add(Integer.parseInt(data.get(i)[j]));
                                    j++;
                                }
                                temp.add(tablePrix);
                                paraProps.add(temp);

                            } else {
                                Groupe g1 = new Groupe(CouleurPropriete.valueOf(savCouleur), savPrixConstruction, paraProps);
                                paraProps.clear();
                                savCouleur = null;
                                recommencer = true;

                            }
                        }
                    } while (recommencer == true);

                    //System.out.println("Propriété :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else if (caseType.compareTo("G") == 0) {
                    Gare temp = new Gare(Integer.parseInt(data.get(i)[1]), data.get(i)[2], this, Integer.parseInt(data.get(i)[3]));
                    carreaux.put(Integer.parseInt(data.get(i)[1]), temp);
                    //System.out.println("Gare :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else if (caseType.compareTo("C") == 0) {
                    Compagnie temp = new Compagnie(Integer.parseInt(data.get(i)[1]), data.get(i)[2], this, Integer.parseInt(data.get(i)[3]));
                    carreaux.put(Integer.parseInt(data.get(i)[1]), temp);
                    //System.out.println("Compagnie :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else if (caseType.compareTo("CT") == 0) {
                    CarreauTirage temp = new CarreauTirage(Integer.parseInt(data.get(i)[1]), data.get(i)[2], this);
                    carreaux.put(Integer.parseInt(data.get(i)[1]), temp);
                    //System.out.println("Case Tirage :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else if (caseType.compareTo("CA") == 0) {
                    CarreauArgent temp = new CarreauArgent(Integer.parseInt(data.get(i)[3]), Integer.parseInt(data.get(i)[1]), data.get(i)[2], this);
                    carreaux.put(Integer.parseInt(data.get(i)[1]), temp);
                    //System.out.println("Case Argent :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else if (caseType.compareTo("CM") == 0) {
                    CarreauMouvement temp = new CarreauMouvement(Integer.parseInt(data.get(i)[1]), data.get(i)[2], this);
                    carreaux.put(Integer.parseInt(data.get(i)[1]), temp);
                    //System.out.println("Case Mouvement :\t" + data.get(i)[2] + "\t@ case " + data.get(i)[1]);
                } else {
                    System.err.println("[buildGamePleateau()] : Invalid Data type");
                }

            }
            Groupe g1 = new Groupe(CouleurPropriete.valueOf(savCouleur), savPrixConstruction, paraProps);

        } catch (FileNotFoundException e) {
            System.err.println("[buildGamePlateau()] : File is not found!");
        } catch (IOException e) {
            System.err.println("[buildGamePlateau()] : Error while reading file!");
        }
    }

    public void sauvegardePartie(String nomFichier) {
        try {
            final FileOutputStream fichier = new FileOutputStream(nomFichier + ".monopoly");
            ObjectOutputStream oos = new ObjectOutputStream(fichier);
            oos.writeObject(this);
            oos.close();
            fichier.close();

        } catch (IOException e) {
            System.out.println("Erreur de fichier");
            e.printStackTrace();
        }

    }

    public Monopoly chargerPartie(String nomFichier) throws IOException {
        Monopoly m = null;
        try {
            FileInputStream fis = new FileInputStream(nomFichier + ".monopoly");
            ObjectInputStream ois = new ObjectInputStream(fis);
            m = (Monopoly) ois.readObject();
            ois.close();
            fis.close();
        } catch (ClassNotFoundException c) {
            System.out.println("Classe non trouvée");
            c.printStackTrace();
        }
        return m;
    }

    public void essai() {
        int i = 1;
        for (Carreau c : carreaux.values()) {
            System.out.println(c.getNomCarreau());
            // System.out.println(CarreauPropriete.class.getName());
            //System.out.println(c.getClass().getSuperclass().getName());
            if (c.getClass().getSuperclass().getName().equals(CarreauPropriete.class.getName())) {
                System.out.println("Pocot");
                CarreauPropriete cr = (CarreauPropriete) c;
                System.out.println(cr.getNumero());
            }
            i++;
        }
    }

    /**
     * Lis le fichier texte en paramètre.
     *
     * @param filename Nom du fichier.
     * @param token Séparateur de données.
     * @return Données.
     * @throws FileNotFoundException Fichier non trouvé.
     * @throws IOException
     */
    private ArrayList<String[]> readDataFile(String filename, String token) throws FileNotFoundException, IOException {
        ArrayList<String[]> data = new ArrayList<String[]>();

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = null;
        while ((line = reader.readLine()) != null) {
            data.add(line.split(token));
        }
        reader.close();

        return data;
    }

    public Monopoly(HashMap<Integer, Carreau> carreaux, Interface interfaceTexte) {
        this.carreaux = carreaux;
        this.interfaceTexte = interfaceTexte;
        joueurs = new ArrayList<>();
    }

    /**
     * Exécute les action d'un tour de jeu d'un joueur.
     *
     ** @param j
     *
     * Joueur.
     * @throws FailliteException Si le joueur ne peut pas payer.
     * @see Joueur
     */
    public void jouerUnCoup(Joueur j) throws FailliteException {
        do {
            j.modifierRejouer(false);
            j.nouveauDernierDés(this.lancerDésAvancer(j));

            interfaceTexte.descriptionJoueurs(j);
            Carreau caseJ = j.getPositionCourante();


            /* try{
             java.lang.reflect.Method method = caseJ.getClass().getMethod("Action",Joueur.class); // On choppe la méthode "Action" de l'objet "caseJ" (si elle existe) sans vraiment savoir de quel type est caseJ. (évite if/else chiants) 
             method.invoke(caseJ, j);
             }catch(NoSuchMethodException e){
             System.out.println("Methode action non trouvée");
             }
             catch (IllegalArgumentException e) {
             } catch (IllegalAccessException e) {
             } catch (java.lang.reflect.InvocationTargetException e) {
             }*/
            if (caseJ instanceof ProprieteAConstruire) {
                ProprieteAConstruire temp = (ProprieteAConstruire) caseJ;
                temp.Action(j);

            } else if (caseJ instanceof Gare) {
                Gare temp = (Gare) caseJ;
                temp.Action(j);
            } else if (caseJ instanceof Compagnie) {
                Compagnie temp = (Compagnie) caseJ;
                temp.Action(j);
            } else if (caseJ instanceof CarreauArgent) {
                CarreauArgent temp = (CarreauArgent) caseJ;
                temp.Action(j);
            } else if (caseJ instanceof CarreauMouvement) {
                CarreauMouvement temp = (CarreauMouvement) caseJ;
                temp.Action(j);
            } else if (caseJ instanceof CarreauTirage) {
                CarreauTirage temp = (CarreauTirage) caseJ;
                temp.Action(j);
            }
        } while (j.getRejouer());
    }

    /**
     * Lance les dés puis fait avancer le joueur.
     *
     * @param j Joueur à faire avancer.
     * @return Résultat des dés.
     *
     */
    public int[] lancerDésAvancer(Joueur j) throws FailliteException {
        int d1;
        int d2;
        int[] resultat = new int[2];
        if (Monopoly.getMethodeAppellante(4).contains("Demo")) {

            d1 = Demo.des.get(Demo.compteurDés);
            d2 = Demo.des.get(Demo.compteurDés + 1);
            resultat[0] = d1;
            resultat[1] = d2;
            Demo.compteurDés += 2;
        } else {
            resultat = this.lancerDés();
            d1 = resultat[0];
            d2 = resultat[1];
        }
        boolean aAvance = false;

        if (j.getPrisonnier() == true && j.getNbCarteLiberePrison() > 0) {
                boolean rep = this.interfaceTexte.UtiliserCarteLibPrison(j);
                if (rep) {
                    j.enleverCarteLiberePrison();
                    j.modifierPrisonnier(false);
                    j.resetNbTourPrison();
                    j.resetNbDouble();
                    j.avancer(d1, d2);
                    aAvance = true;
                }

            }if(!aAvance){
                if (d1 == d2) {
                    j.addNbDouble();
                    if (j.getPrisonnier() == true) {
                        j.modifierPrisonnier(false);
                        j.resetNbTourPrison();
                        j.modifierRejouer(true);
                        j.avancer(d1, d2);
                        j.resetNbDouble();

                    } else {
                        if (j.getNbDouble() >= 3) {
                            j.modifierPrisonnier(true);
                            j.modifierPositionCourante(this.carreaux.get(11));
                            j.resetNbDouble();
                        } else {
                            j.modifierRejouer(true);
                            j.avancer(d1, d2);
                        }
                    }
                } else {
                    if (j.getPrisonnier() == true) {
                        j.addNbTourPrison();
                        if (j.getNbTourPrison() >= 3) {
                            j.debite(50);
                            j.modifierPrisonnier(false);
                            j.resetNbTourPrison();
                            j.avancer(d1, d2);
                        }
                    } else {
                        j.resetNbDouble();
                        j.avancer(d1, d2);
                    }
                }
            
    
    }
    return resultat;
    }

    /**
     * Lance les dés.
     *
     * @return Résultat des dés.
     */
    public int[] lancerDés() {
        int d[] = new int[2];
        java.util.Random r = new java.util.Random();

        for (int i = 0; i < d.length; i++) {
            d[i] = 1 + r.nextInt(5);
        }

        return d;
    }

    /**
     * Renvoie le nombre de maison disponible dans la partie.
     *
     * @return Nombre de maison.
     */
    public int getNbMaisons() {
        return nbMaisons;
    }

    /**
     * Met à jour le nombre de maison disponible.
     *
     * @param nbMaisons Nombre de maison.
     */
    private void setNbMaisons(int nbMaisons) {
        this.nbMaisons = nbMaisons;
    }

    public void ajouterMaisons(int nbMaisons) {
        this.setNbMaisons(this.getNbMaisons() + nbMaisons);
    }

    /**
     * Retourne le nombre d'hôtels disponibles dans la partie.
     *
     * @return Nombre d'hôtels.
     */
    public int getNbHotels() {
        return nbHotels;
    }

    private void setNbHotels(int nbHotels) {
        this.nbHotels = nbHotels;
    }

    public void ajouterHotels(int nbHotels) {
        this.setNbHotels(this.getNbHotels() + nbHotels);
    }

    /**
     * Renvoie toutes les cases du plateau.
     *
     * @return numéro + Carreau
     *
     * @see #carreaux
     */
    public HashMap<Integer, Carreau> getCarreaux() {
        return carreaux;

    }

    /**
     * Renvoie les joueurs de la partie.
     *
     * @return ArrayList des joueurs de la partie.
     */
    public ArrayList<Joueur> getJoueurs() {
        return joueurs;
    }

    /**
     * Met les joueurs de la partie à jour.
     *
     * @param joueurs ArrayList des nouveaux Joueurs.
     */
    private void setJoueurs(ArrayList<Joueur> joueurs) {
        this.joueurs = joueurs;

    }

    /**
     * Renvoie l'interface utilisée.
     *
     * @return Interface utilisée.
     */
    public Interface getInterfaceTexte() {
        return interfaceTexte;
    }

    /**
     * Met à jour l'interface de jeu.
     *
     * @param interfaceTexte Interface à utiliser.
     */
    private void setInterfaceTexte(Interface interfaceTexte) {
        this.interfaceTexte = interfaceTexte;
    }

    public void modifierInterfaceTexte(Interface interfaceTexte) {
        this.setInterfaceTexte(interfaceTexte);
    }

    /**
     * Ajoute un joueur à la partie.
     *
     * @param nom Nom du joueur.
     */
    public void addJoueur(String nom) {
        this.joueurs.add(new Joueur(nom, this));

    }

    /**
     * Permet de réoganiser l'ArrayList joueur pour que le joueur corrrespondant
     * à l'index en paramètre joue en premier.
     *
     * @param index Index correspondant au joueur.
     * @see #lancerDésAvancer(Jeu.Joueur)
     */
    public void reorgansationJoueurs(int index) {
        ArrayList<Joueur> temp = new ArrayList();
        int i = 0;
        while (index + i < this.getJoueurs().size()) {
            temp.add(this.getJoueurs().get(index + i));
            i++;
        }
        i = 0;
        while (i < index) {
            temp.add(this.getJoueurs().get(i));
            i++;
        }
        this.setJoueurs(temp);
    }

    /**
     * Enlève une maison à celles disponibles.
     */
    public void subMaison() {
        this.setNbMaisons(nbMaisons - 1);
    }
    //On enlève une maison disponible aux joueur pour cette partie.

    /**
     * Enlève un hôtel à ceux disponibles.
     */
    public void subHotel() {
        this.setNbHotels(nbHotels - 1);
    }

    //Idem mais avec Hotels.
    /**
     * Retourne vrai si la partie possède encore des maisons.
     *
     * @return Statut booléen.
     */
    public boolean hasMaison() {
        return this.getNbMaisons() >= 1;
    }
    //On regarde si Il reste des Maison disponible aux joueurs.

    /**
     * Retourne vrai si la partie dispose encore d'hôtels.
     *
     * @return Statut booléen.
     */
    public boolean hasHotel() {
        return this.getNbHotels() >= 1;
    }

    //idem mais avec hotels.
    /**
     * Ajoute une case au plateau.
     *
     * @param i Numéro du carreau.
     * @param c Carreau en lui-même.
     */
    public void ajouterCarreau(Integer i, Carreau c) {
        carreaux.put(i, c);
    }

    /**
     * Supprime les joueurs de cette partie.
     */
    public void reinitialiserJoueurs() {
        this.joueurs.clear();
    }

    public Carte tirerCarte(TypeCarte t) {
        if (t.equals(TypeCarte.Chance)) {
            Carte pioche = this.cartesChance.pollFirst();
            if (!(pioche instanceof CarteLiberePrison)) {
                this.cartesChance.addLast(pioche);
            }

            return pioche;
        } else {
            Carte pioche = this.cartesCommunaute.pollFirst();
            if (!(pioche instanceof CarteLiberePrison)) {
                this.cartesCommunaute.addLast(pioche);
            }
            return pioche;

        }
    }

    /**
     * Mélange les cartes d'un paquet.
     *
     * @param paquet Le paquet à mélanger.
     * @see Carte
     */
    public void melangerCartes(LinkedList<Carte> paquet) {
        Random r = new Random();

        for (int i = 0; i < paquet.size(); i++) {
            this.echanger(paquet, r.nextInt(paquet.size()), r.nextInt(paquet.size()));
        }
    }

    /**
     * Echange deux cartes.
     *
     * @param paquet Le paquet de carte.
     * @param i L'indice de la première carte à échanger.
     * @param j L'indice de la seconde carte à échanger.
     */
    private void echanger(LinkedList<Carte> paquet, int i, int j) {

        Carte temp;

        temp = paquet.get(i);

        paquet.set(i, paquet.get(j));

        paquet.set(j, temp);

    }

    /**
     * Renvoie la méthode et la classe qui ont appellé celle-ci.
     *
     * @return Classe.Méthode appelantes.
     */
    public static String getMethodeAppellante(int level) {
        return trace(Thread.currentThread().getStackTrace(), level);
    }

    /**
     * Analyse trace
     *
     * @param e Trace actuelle.
     * @param level Niveau auquel remonter.
     * @return Méthode appellante.
     */
    private static String trace(StackTraceElement[] e, int level) {
        if (e != null && e.length >= level) {
            StackTraceElement s = e[level];
            if (s != null) {

                return s.getClassName() + "." + s.getMethodName();
            }
        }
        return null;
    }

    /**
     * Ajouter une carte libéré de prison à celles possédées par les joueurs.
     *
     * @param clp Carte à ajouter.
     */
    public void ajouterCarteSortieJeu(CarteLiberePrison clp) {
        this.carteSortiesJeu.add(clp);
    }

    /**
     * Remet une carte libéré de prison dans le jeu.
     */
    public void remettreCarteJeu() {
        CarteLiberePrison clp = this.carteSortiesJeu.poll();
        if (clp.getType() == TypeCarte.Chance) {
            this.cartesChance.addLast(clp);

        } else {
            this.cartesCommunaute.addLast(clp);
        }
    }

    /**
     * Retourne les cartes chance.
     *
     * @return Liste cartes chance.
     */
    public LinkedList<Carte> getCartesChance() {
        return (LinkedList<Carte>) cartesChance.clone();
    }

    /**
     * Retourne les cartes communauté.
     *
     * @return Liste cartes commuanuté.
     */
    public LinkedList<Carte> getCartesCommunaute() {
        return (LinkedList<Carte>) cartesCommunaute.clone();
    }

    public void MettreAuDessusPaquet(TypeCarte tc, Carte c) {
        if (tc.equals(TypeCarte.Chance)) {
            this.cartesChance.addFirst(c);
        } else {
            this.cartesCommunaute.addFirst(c);
        }
    }

}
