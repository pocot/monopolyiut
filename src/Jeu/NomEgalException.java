
package Jeu;

/**
 * Exception lancée si lors du choix des noms des joueurs, un joueur à le même nom qu'un autre.
 * @see Interface#inscriptionJoueurs
 * 
 */
public class NomEgalException extends Exception {
    

    /**
     * Constructeur de <code>NomEgalException</code>.
     * 
     */
    public NomEgalException() {
    }

}
