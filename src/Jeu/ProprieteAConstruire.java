package Jeu;

/**
 * <b>ProprieteAConstruire est la classe représentant les case de proprietes
 * physique où l'on peut y constuire 4 maisons ou 1 hotel par case.</b>
 * <p>
 * Une proprieteAConstruire est caractérisée par :
 * <ul>
 * <li> Un numéro de Carreau. </li>
 * <li> Un nom de ProprieteAConstuire. </li>
 * <li> Une partie en cours (Monopoly).</li>
 * <li> Le prix d'achat de la compagnie.</li>
 * <li> La table de prix spécifique à la propriete</li>
 * <li> Le groupe de couleur de la propriete</li>
 * </ul>
 * </p>
 * <p>
 * <i>Cette classe hérite de la classe CarreauPropriete.</i>
 * </p>
 *
 * @see CarreauPropriete
 * @see Carreau#numero
 * @see Carreau#nomCarreau
 * @see Carreau#monopoly
 * @see Groupe
 */
import java.util.ArrayList;

public class ProprieteAConstruire extends CarreauPropriete {

    private int nbMaisons;
    private int nbHotels;
    private ArrayList<Integer> loyerMaison;
    private ArrayList<Integer> tablePrix;
    private Groupe groupePropriete;

    public ProprieteAConstruire(int numero, String nomCarreau, Monopoly monopoly, int prixAchat, ArrayList<Integer> tablePrix, Groupe groupePropriete) {
        super(numero, nomCarreau, monopoly, prixAchat);
        this.tablePrix = tablePrix;
        this.groupePropriete = groupePropriete;
        this.nbMaisons = 0;
        this.nbHotels = 0;
    }

    /**
     * Renvoie le loyer de cette proprieteAConstruire.
     *
     * @param jProprio Le proprietaire de cette propriété.
     * @param j Le joueur tombant sur cette propriété.
     * @return
     * @see #tablePrix
     */
    @Override
    public int getLoyer(Joueur jProprio, Joueur j) {
        if (this.nbHotels == 1) {
            return this.tablePrix.get(5);
        } else {
            if (j.hasFullGroupe(this)) {
                return 2 * this.tablePrix.get(this.nbMaisons);
            } else {
                return this.tablePrix.get(this.nbMaisons);
            }
        }
    }

    /**
     * Retourne le nombre de maison présent sur la propriete.
     *
     * @return Le nombre de maison présent sur la propriete.
     */
    public int getNbMaisons() {
        return nbMaisons;
    }

    /**
     * Change la valeur du nombre de maisons présentes sur la propriete du
     * nombre de maison en paramètre.
     *
     * @param nbMaisons Le nouveau nombre de maison.
     * @see #nbMaisons
     */
    private void setNbMaisons(int nbMaisons) {
        this.nbMaisons = nbMaisons;
    }

    public void modifierNbMaisons(int nbMaisons) {
        this.setNbMaisons(nbMaisons);
    }

    /**
     * Retourne le nombre d'hotel présent sur la propriete.
     *
     * @return Le nombre d'hotel présent sur la propriete.
     */
    public int getNbHotels() {
        return nbHotels;
    }

    /**
     * Change la valeur du nombre d'hotel présentes sur la propriete du nombre
     * d'hotel en paramètre.
     *
     * @param nbHotels Le nouveau nombre de maison.
     * @see #nbHotels
     */
    private void setNbHotels(int nbHotels) {
        this.nbHotels = nbHotels;
    }

    public void modifierNbHotels(int nbHotels) {
        this.setNbHotels(nbHotels);
    }

    /**
     * Retourne un ArrayList de prix.
     *
     * @return L'ArrayList de prix.
     */
    public ArrayList<Integer> getLoyerMaison() {

        return loyerMaison;

    }

    public ArrayList<Integer> getTablePrix() {
        return tablePrix;

    }

    /**
     * Retourne le groupe de la propriete.
     *
     * @return Le groupe de la propriete.
     */
    public Groupe getGroupePropriete() {
        return groupePropriete;
    }

    /**
     * Regarde si le joueur peut ajouter une maison ou un hotel sur une
     * propriete qui lui appartient.
     *
     * @param j Le joueur qui souhaite construire.
     */
    public void construire(Joueur j) {

        ArrayList<ProprieteAConstruire> props = this.getGroupePropriete().getProprietes();//On recupere toutes les props du groupe

        ArrayList<ProprieteAConstruire> paps = new ArrayList<>();
        //Il faut de meilleurs noms pour ces ArrayList
        //paps contient les props dont le nombre de maisons est inférieur au nombre de maisons maximum dans les propriete du groupe,
        ArrayList<ProprieteAConstruire> paps2 = new ArrayList<>();
        //paps contient les props dont le nombre de maisons est égal au nombre de maisons maximum dans les propriete du groupe,
        int nbMaisonMax = 0;
        for (ProprieteAConstruire pap : props) {
            if (nbMaisonMax < pap.getNbMaisons()) {
                nbMaisonMax = pap.getNbMaisons();
            }
        }
        for (ProprieteAConstruire p : props) {
            if (nbMaisonMax > p.getNbMaisons() && p.getNbHotels() != 1) {
                paps.add(p);
            } else if (nbMaisonMax == p.getNbMaisons() && p.getNbHotels() != 1) {
                paps2.add(p);
            }
            //Il faut donner au joueur les props qui ont un nombre de maison le moins grands
        }
        if (paps.isEmpty()) {
            //Ici si toutes les props ont le même nombre de maison sur leur terrain alors on choisi le Arraylist de props paps2
            //methode de Interface pour afficher les propriété au joueurs et choisir les props de paps2
            this.getMonopoly().getInterfaceTexte().construction(paps2, j);
        } else {
            //Ici si une ou plusieurs des props à un nombre de maison supérieur aux autres alors ont choisi le Arraylist de props paps
            //methode de Interface pour afficher les propriété au joueurs et choisir les props de paps
            this.getMonopoly().getInterfaceTexte().construction(paps, j);
        }

    }

    /**
     * Regarde si il y a assez de maisons où d'hotel pour construire pour
     * ensuite ajouter une maison ou un hotel sur la propriete choisie par le
     * joueur enlève la maison ou l'hotel ou la maison de la caisse de la
     * partie.
     *
     * @param j Le joueur qui souhaite construire.
     * @param monopoly La partie en cours.
     */
    public void construire(Joueur j, Monopoly monopoly) {

        // On regarde si il reste des maisons disponible.
        if (this.getNbMaisons() < 4 && this.getNbHotels() < 1) {
            this.addMaison();
            //On ajoute la maison à la propriete.
            monopoly.subMaison();
            //On enlève une maison disponible au Monopoly.
            try {
                j.debite(this.getGroupePropriete().getPrixConstruction());
            } catch (FailliteException f) {        //le catch est impossible à atteindre. Pas besoin de traitements.

            }
        } else if (this.getNbMaisons() == 4 && this.getNbHotels() < 1) {
            this.addHotel();
            monopoly.subHotel();
            this.modifierNbMaisons(0);
            monopoly.ajouterMaisons(4);

            try {
                j.debite(this.getGroupePropriete().getPrixConstruction());
            } catch (FailliteException f) {    //le catch est impossible à atteindre. Pas besoin de traitements.

            }
        }
    }

    /**
     * Ajoute une maison sur la propriete.
     *
     * @see #nbMaisons
     */
    public void addMaison() {
        this.setNbMaisons(nbMaisons + 1);
    }

    /**
     * Ajoute un hotel sur la propriete.
     *
     * @see #nbHotels
     */
    public void addHotel() {
        this.setNbHotels(nbHotels + 1);
    }

    /**
     * <p>
     * Définie l'action à effectuer quand on tombe sur cette
     * ProprieteAConstruire.</p>
     * <p>
     * Ici, si le propriétaire n'est pas défini, le joueur qui est tombé sur ce
     * carreau peut l'acheter.
     * </p><p>
     * Si le propriétaire est définie, le joueur tombé sur ce CarreauPropriété
     * doit payer le loyer </p>
     * </p>
     * <p>
     * Si le joueur est le proprietaire, le joueur peut alors construire sur le
     * groupe auquel appartient la ProprieteAConstruire
     * </p>
     *
     * @param j Joueur tombé sur la case.
     * @throws Jeu.FailliteException
     * @see Joueur
     * @see #achatPropriete(Jeu.Joueur)
     * @see #calculLoyer(Jeu.Joueur, Jeu.Joueur)
     * @see Joueur#credite(int)
     * @see Joueur#payerLoyer(int)
     * @see #construire(Jeu.Joueur)
     */
    @Override
    public void Action(Joueur j) throws FailliteException {
        //Override de Action car dans le cas d'une PAP le joueur peut choisir de construire
        super.Action(j);
        Boolean peutConstruire = true;
        Boolean resteMaisons = true;
        Boolean resteHotels = true;
        Boolean hasHotels;
        Joueur jproprio = this.getProprietaire();
        int hasHotel = 0;
        int cash = j.getCash();
        int prixConstruction = this.getGroupePropriete().getPrixConstruction();
        int cashManquant = 0;
        int propManquante = 0;
        ArrayList<ProprieteAConstruire> props = this.getGroupePropriete().getProprietes();//On recupere toutes les props du groupe
        ArrayList<ProprieteAConstruire> propsTemp = new ArrayList<>();
        boolean first = true;
        Boolean encore = true;

        if (j == jproprio) {
            do {

                if (cash < prixConstruction) {
                    cashManquant = (cash - prixConstruction) * -1;
                    peutConstruire = false;
                }

                if (!j.hasFullGroupe(this)) {
                    //on regarde si le joueur possède bien toutes les propriete du groupe de la propriete où il est.
                    for (ProprieteAConstruire p : j.getProprietesAConstruire()) {
                        if (p.getGroupePropriete() == this.getGroupePropriete()) {
                            propsTemp.add(p);
                        }
                    }

                    propManquante = (propsTemp.size() - props.size()) * -1;
                    peutConstruire = false;
                }

                if (!this.getMonopoly().hasMaison()) {
                    resteMaisons = false;
                    peutConstruire = false;
                }

                if (!this.getMonopoly().hasHotel()) {
                    resteHotels = false;
                    peutConstruire = false;
                }
                hasHotel = 0;
                for (ProprieteAConstruire p : props) {
                    if (p.getNbHotels() >= 1) {
                        hasHotel++;
                    }
                }

                if (hasHotel == this.getGroupePropriete().getProprietes().size()) {
                    hasHotels = true;
                    peutConstruire = false;
                } else {
                    hasHotels = false;
                }
                if (first) {
                    encore = this.getMonopoly().getInterfaceTexte().demandeConstruction(peutConstruire, cashManquant, propManquante, resteMaisons, resteHotels, hasHotels);
                    if (encore) {                        
                        //Si le joueur est le propriétaire de la propriete alors il peut construire.
                        this.construire(j);
                    }
                    first = false;
                } else {
                    encore = this.getMonopoly().getInterfaceTexte().demandeEncoreConstruction(peutConstruire, cashManquant, propManquante, resteMaisons, resteHotels, hasHotels);
                    if (encore) {
                        this.construire(j);
                    }
                }
                first = false;

            } while (encore);
        }
    }
}
