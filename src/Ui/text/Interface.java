package Ui.text;

import Jeu.*;
import Jeu.Carreau;
import Jeu.CarreauPropriete;
import Jeu.Gare;
import Jeu.Joueur;
import Jeu.Monopoly;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import static java.lang.Math.abs;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * La classe interface a pour rôle d'afficher toutes les informations à
 * destination de l'utilisateurs et de récupérer celles demandées à celui-ci.
 * Une interface est constitué d 'un monpoly, représentant la partie en cours.
 *
 * @see Monopoly
 */
public class Interface implements Serializable {

    /**
     * La partie associé à cette interface.
     *
     * @see Monopoly
     */
    public Monopoly monopoly;

    /**
     * Constructeur de <code>Interface</code>.
     *
     * @param monopoly La partie en cours.
     */
    public Interface(Monopoly monopoly) {
        this.monopoly = monopoly;
    }

    /**
     * Affiche aux joueurs la faillite du joueur en paramètre.
     *
     * @param j Le joueur en faillite.
     * @see FailliteException
     */
    public void faillite(Joueur j) {
        System.out.println("Le joueur " + j.getNomJoueur() + " est en faillite!");
        System.out.println("Toutes les maisons et hôtels sur ses propriétés ont été rendus de nouveau disponibles!");
        System.out.println("Toutes ses propriétés ont aussi été rendues de nouveau disponibles!");

    }

    public void chtitesEtoiles() {
        System.out.println("\n__________________________________________________________\n");
        System.out.println("__________________________________________________________\n");
    }

    /**
     * Affiche les options du menu principal du jeu, après avoir décidé de
     * lancer une nouvelle partie. Récupère le choix de l'utilisateur et appelle
     * les méthodes correspondantes.
     *
     * @see Main
     * @see #inscriptionJoueurs()
     */
    public void menuDebutJeu() {
        chtitesEtoiles();
        System.out.println("Choississez une option :");
        System.out.println("1. Inscrire les joueurs");
        System.out.println("2. Commencer le jeu");
        System.out.println("3. Quitter");
        System.out.println("Votre choix :\t");

        int choix = 0;
        boolean OK = false;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                choix = sc.nextInt();
                if (choix > 3 || choix < 1) {
                    throw new InputMismatchException();
                }
                OK = true;

            } catch (InputMismatchException ime) {
                System.out.println("Vous devez saisir un entier entre 1 et 3 !");
                sc.nextLine();
            }
        } while (!OK);

        switch (choix) {
            case 1: {
                this.inscriptionJoueurs();
                System.out.println("");
            }
            break;
            case 2: {
                if (this.monopoly.getJoueurs().isEmpty()) {
                    System.out.println("Vous n'avez pas inscris de joueurs pour le moment. Veuillez en inscrire avant de commencer la partie.");
                    this.menuDebutJeu();
                }

            }
            break;
            case 3: {
                System.exit(0);
            }
            break;

        }

    }

    /**
     * Affiche le menu permettant de choisir de Démarrer une nouvelle partie ou
     * d'en charger une. Récupère le choix de l'utilisateur et renvoi un String
     * représentant le nom de la partie à charger. Si l'utilisateur a choisi de
     * démarrer une nouvelle partie, la méthode renvoie "-1".
     *
     * @return Le nom du fichier à charger ou "-1".
     * @see Monopoly#chargerPartie(java.lang.String)
     */
    public String ChargerouRedemarrer() {
        System.out.println("Voulez-vous :");
        System.out.println("1. Demarrer une nouvelle partie");
        System.out.println("2. Charger une partie");

        int choix = 0;
        boolean OK = false;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                choix = sc.nextInt();
                if (choix > 2 || choix < 1) {
                    throw new InputMismatchException();
                }
                OK = true;

            } catch (InputMismatchException ime) {
                System.out.println("Vous devez saisir un entier entre 1 et 2 !");
                sc.nextLine();
            }
        } while (!OK);

        String nomFichier = null;

        if (choix == 2) {
            System.out.println("Quel est le nom du fichier à charger ?");
            sc.nextLine();
            nomFichier = sc.nextLine();
        }
        if (choix == 1) {
            nomFichier = "-1";
        }

        return nomFichier;

    }

    /**
     * Affiche les informations concernant une propriété (prix,couleur(si
     * disponible)); puis demande à l'utilisateur s'il veut l'acheter.
     *
     * @param c La propriété dont on veut afficher les paramètres.
     * @return Volonté d'achat de l'utilisateur: True = Volonté d'acheter. False
     * = Ne veut pas acheter.
     * @see CarreauPropriete
     */
    public boolean descriptionPropriete(CarreauPropriete c) {
        if (c instanceof ProprieteAConstruire) {
            ProprieteAConstruire c2 = (ProprieteAConstruire) c;
            System.out.println("Voulez-vous acheter " + c2.getNomCarreau() + ", une propriété " + c2.getGroupePropriete().getCouleur() + " à " + c2.getPrixAchatPropriete() + "€ ? \t");
        } else {
            System.out.println("Voulez-vous acheter " + c.getNomCarreau() + " à " + c.getPrixAchatPropriete() + "€ ? \t");
        }
        Scanner sc = new Scanner(System.in);
        String rep = null;
        boolean OK = false;

        do {
            try {
                rep = sc.nextLine();
                if (!rep.toLowerCase().equals("oui") && !rep.toLowerCase().equals("non")) {
                    throw new IOException();
                }
                OK = true;

            } catch (IOException ioe) {
                System.out.println("La réponse doit être oui ou non! Recommencez : \t");
            }
        } while (OK == false);

        if (rep.toLowerCase().equals("oui")) {
            System.out.println("Achat effectué.");
        } else {
            System.out.println("Achat refusé.");
        }

        return (rep.toLowerCase().equals("oui"));
    }

    /**
     * Affiche le nécéssaire pour permettre à l'utilisateur de choisir le nombre
     * de joueurs et les noms de ceux-ci.
     *
     * @see Joueur
     */
    public void inscriptionJoueurs() {
        Scanner sc = new Scanner(System.in);
        System.out.println("\nEntrez le nombre de joueurs :\t");
        boolean ok;
        int nbJoueurs = 0;
        do {
            try {
                nbJoueurs = sc.nextInt();                          /*Ici on fait une saisie sécurisée du nombre de joueurs*/

                if (nbJoueurs < 2 || nbJoueurs > 6) {
                    throw new InputMismatchException();
                }
                ok = true;
            } catch (InputMismatchException ime) {
                System.out.println("Le nombre de joueur est un chiffre compris entre 2 et 6.\nRecommencez :\t");
                ok = false;
                sc.nextLine();
            }
        } while (ok == false);

        sc.nextLine();

        String nomJ = null;
        /*Ajout Joueurs*/
        for (int i = 1; i <= nbJoueurs; i++) {
            do {
                System.out.println("\nQuel est le nom du joueur " + i + "?");
                try {
                    nomJ = sc.nextLine();                                   /*Saisie sécurisée du nom*/

                    if (nomJ.length() > 20 || nomJ.length() == 0) {
                        throw new IOException();
                    }
                    for (Joueur j : this.monopoly.getJoueurs()) {
                        if (nomJ.equals(j.getNomJoueur())) {
                            throw new NomEgalException();
                        }
                    }
                    ok = true;
                } catch (IOException ioe) {
                    System.out.println("Votre nom doit contenir entre 1 et 20 caractères !\nRecommencez :\t");
                    ok = false;
                } catch (NomEgalException nee) {
                    System.out.println("Un autre joueur porte déjà ce nom. Choississez-en un autre.");
                    ok = false;
                }
            } while (ok == false);
            this.monopoly.addJoueur(nomJ);

        }

    }

    public void afficheManqueArgentProp(Joueur j, int prix) {
        System.out.println("Il vous manque " + (prix - j.getCash()) + "€ pour acheter cette propriété");
    }

    /**
     * Affiche la sélection du joueur qui jouera en premier via un lancé de dés.
     *
     * @see Monopoly#lancerDés()
     */
    public void determinerQuiCommence() {

        Joueur joueurQuiCommence = null;
        int meilleurRes = 0, resDés;
        int[] tableRes;
        System.out.println("Déterminons qui commence.");
        Scanner sc = new Scanner(System.in);
        for (Joueur j : this.monopoly.getJoueurs()) {
            System.out.println(j.getNomJoueur() + ", appuyez sur entrée pour lancer les dés.");
            sc.nextLine();
            tableRes = this.monopoly.lancerDés();
            resDés = tableRes[1] + tableRes[0];
            System.out.println(j.getNomJoueur() + " jette les dés..." + resDés + " !");

            if (resDés > meilleurRes) {
                meilleurRes = resDés;
                joueurQuiCommence = j;

            }
        }
        System.out.println("Avec un jet de " + meilleurRes + ' ' + joueurQuiCommence.getNomJoueur() + " commence");
        this.monopoly.reorgansationJoueurs(monopoly.getJoueurs().indexOf(joueurQuiCommence));
        System.out.println("L'ordre de jeu sera donc : ");
        int i = 1;
        for (Joueur j : this.monopoly.getJoueurs()) {
            System.out.println(i + " : " + j.getNomJoueur());
            i++;
        }
        int choix = 0;
        boolean OK = false;
        do {
            chtitesEtoiles();
            System.out.println("Choississez une option");
            System.out.println("1. Commencez la partie");
            System.out.println("2. Recommencer la saisie des joueurs");

            //sc.close();
            //Scanner sc2 = new Scanner(System.in);
            do {
                try {
                    choix = sc.nextInt();
                    if (choix < 1 || choix > 2) {
                        throw new InputMismatchException();
                    }
                    OK = true;
                } catch (InputMismatchException ime) {
                    System.out.println("Vous devez saisir un entier entre 1 et 2 !");
                    sc.next();

                }
            } while (!OK);
            OK = false;
            switch (choix) {
                case 1: {
                    OK = true;
                }
                break;
                case 2: {
                    this.monopoly.reinitialiserJoueurs();
                    this.inscriptionJoueurs();
                }
                break;
            }
        } while (!OK);
    }

    private static void intitulé(String txt) {
        System.out.println();
        for (int i = 1; i <= txt.length() + 4; i++) {
            System.out.print("*");
        }
        System.out.println();
        System.out.println("* " + txt + " *");
        for (int i = 1; i <= txt.length() + 4; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    /**
     * Affiche le menu qui pour chaque tour, décrit l'état du joueur en
     * paramètre, puis des autres. Il permet ensuite de choisir entre lancer les
     * dés, sauvegarder la partie ou quitter la partie.
     *
     * @param j Joueur dont le tour est en cours.
     * @throws FailliteException Si le joueur décide de quitter la partie, il
     * est mis en faillite.
     * @see FailliteException
     * @see Monopoly#lancerDésAvancer(Jeu.Joueur)
     */
    public void descriptionJoueurs(Joueur j) throws FailliteException {
        Scanner sc = new Scanner(System.in);
        int choix = 0;
        boolean ok = false;
        if (!(Monopoly.getMethodeAppellante(4).contains("Jeu.Demo.coupScenario"))) {

            chtitesEtoiles();
            intitulé("C'est au tour de " + j.getNomJoueur());
            System.out.println("\n1) Lancer les dés");
            System.out.println("2) Sauvegarder et arrêter la partie");
            System.out.println("3) Quitter la partie");
            do {
                try {
                    choix = sc.nextInt();
                    if (choix < 1 || choix > 3) {
                        throw new InputMismatchException();
                    }
                    ok = true;
                } catch (InputMismatchException ime) {
                    System.out.println("Vous devez entrer un entier entre 1 et 3");
                    sc.nextLine();
                }
            } while (ok == false);
            ok = false;

        } else {
            choix = 1;
        }
        switch (choix) {
            case 1:

                boolean done = false;
                for (Joueur autreJoueur : this.monopoly.getJoueurs()) {
                    if (autreJoueur != j && !autreJoueur.isFaillite()) {
                        if (!done) {
                            intitulé("Infos Joueurs");
                            done = true;
                        }
                        System.out.println("\n-->" + autreJoueur.getNomJoueur() + " lui, est à la case n°" + autreJoueur.getPositionCourante().getNumero() + " : " + autreJoueur.getPositionCourante().getNomCarreau() + ".\nIl a " + autreJoueur.getCash() + " euros et possède : ");
                        if (autreJoueur.getGares().isEmpty() && autreJoueur.getCompagnies().isEmpty() && autreJoueur.getProprietesAConstruire().isEmpty()) {
                            System.out.print("Rien\n\n");
                        } else {
                            for (Gare g : autreJoueur.getGares()) {
                                System.out.println(g.getNomCarreau());
                            }
                            for (Compagnie c : autreJoueur.getCompagnies()) {
                                System.out.println(c.getNomCarreau());
                            }
                            for (ProprieteAConstruire pac : autreJoueur.getProprietesAConstruire()) {
                                System.out.println(pac.getNomCarreau() + " propriété " + pac.getGroupePropriete().getCouleur() + " avec " + pac.getNbMaisons() + " maisons et " + pac.getNbHotels() + " hôtels.");

                            }
                        }

                    }

                }
                intitulé(j.getNomJoueur() + " lance les dés");
                System.out.println("\nVous avez fait un " + j.getDernierDés()[0] + " et un " + j.getDernierDés()[1] + " !");
                if (j.getDernierDés()[0] == j.getDernierDés()[1] && !j.getPrisonnier() && j.getPositionCourante().getNumero() != 31) {
                    System.out.println("Vous pouvez donc rejouer !");
                }
                int placeDAvant = j.getPositionCourante().getNumero() - j.getDernierDés()[0] - (j.getDernierDés()[1]);

                if (placeDAvant <= 0) {
                    placeDAvant = 40 + placeDAvant;
                }
                if (j.getNbDouble() == 2) {
                    System.out.println("Attention ! Encore un et vous irez en prison !");
                }                

                if (placeDAvant > j.getPositionCourante().getNumero() && !j.getPrisonnier()) {
                    System.out.println("C'est jour de paye ! recevez 200€ !");
                }

                if (j.getPrisonnier() && j.getDernierDés()[0] != j.getDernierDés()[1]) {
                    System.out.println("Sans double vous ne sortez pas !");
                }

                if (j.getNbTourPrison() != 0 && j.getDernierDés()[0] == j.getDernierDés()[1] && j.getPrisonnier()) {
                    System.out.println("Vous êtes libre !");
                }

                Carreau caseJ = j.getPositionCourante();
                System.out.println("Case actuelle : " + caseJ.getNumero() + " - " + caseJ.getNomCarreau());

                System.out.println("Vous possèdez " + j.getCash() + "€\n");
                if (j.getPrisonnier() == true && j.getNbTourPrison() != 0) {
                    System.out.println("C'est votre tour n°" + j.getNbTourPrison() + " en prison.");
                }
                if (j.getNbTourPrison() == 2 && j.getDernierDés()[0] != j.getDernierDés()[1]) {
                    System.out.println("Au prochain tour, vous paiyerez pour sortir.");
                }
                break;

            case 2:
                String nomFichier = null;
                Scanner sc2 = new Scanner(System.in);
                System.out.println("Entrez le nom du fichier de sauvegarde");

                do {
                    try {
                        nomFichier = sc2.nextLine();
                        if (nomFichier.length() < 3 || nomFichier.length() > 20 || nomFichier.contains(".")) {
                            throw new InputMismatchException();
                        }
                        File f = new File(nomFichier + ".monopoly");
                        if (f.exists()) {
                            throw new IOException();
                        }
                        ok = true;

                    } catch (InputMismatchException ime) {
                        System.out.println("Le nom du fichier doit contenir entre 3 et 20 caractères et il ne doit pas y avoir d'extension. Recommencez:\t");
                    } catch (IOException ioe) {
                        System.out.println("Ce fichier existe déjà. Recommencez :\t ");
                    }
                } while (!ok);
                this.monopoly.reorgansationJoueurs(this.monopoly.getJoueurs().indexOf(j));
                this.monopoly.sauvegardePartie(nomFichier);
                System.out.println("Sauvegarde terminée.");
                // 
                System.exit(0);
                break;

            case 3:
                throw new FailliteException();
            //break;

        }

    }

    /**
     * Message informant de la paye de la case départ.
     */

    /**
     * Affiche le fait qu'il n'y ait plus de maisons disponibles pour la partie
     * en cours.
     *
     * @see Monopoly#nbMaisons
     */
    public void manqueMaisons() {
        System.out.println("Il ne reste plus de maisons disponibles pour pouvoir construire");
    }

    /**
     * Affiche le fait qu'il n'y ait plus d'hôtels disponibles pour la partie en
     * cours.
     *
     * @see Monopoly#nbHotels
     */
    public void manqueHotels() {
        System.out.println("Il ne reste plus d' hotels disponibles pour pouvoir construire");
    }

    /**
     * Si le joueur peut construire, demande au joueur s'il le veut. Dans le cas
     * contraire, affiche les raisons pour lesquelles ce n'est pas possible.
     *
     * @param peutConstruire Indique si le joueur peut construire. True = Le
     * joueur peut construire. False = Le joueur ne peut pas construire.
     * @param cashManquant Nombre représentant l'argent manquant au joueur pour
     * pouvoir construire. Si le joueur possède assez d'argent, ce paramètre
     * vaut 0.
     * @param propManquante Nombre représentant le nombre de propriété qu'il
     * manque au joueur pour pouvoir construire. Si le joueur a toutes les
     * propriétés requises, ce paramètre vaut 0.
     * @param resteMaisons Indique s'il reste des maisons disponibles dans la
     * partie en cours. True = Maisons disponibles. False = Pas de maisons
     * disponibles.
     * @param resteHotels Indique s'il reste des hôtels disponibls dans la
     * partie en cours. True = Hôtels disponibles. False = Hôtels non
     * disponibles.
     * @param hasHotels Indique si le joueur possède des hôtels sur toutes ses
     * propriétés.
     *
     * @return Boolean représentant si le joueur veut construire. True = Le
     * joueur veut construire. False = Le joueur ne veut pas construire.
     *
     * @see Monopoly#nbHotels
     * @see Monopoly#nbMaisons
     * @see ProprieteAConstruire
     * @see ProprieteAConstruire#construire(Jeu.Joueur, Jeu.Monopoly)
     */
    public Boolean demandeConstruction(Boolean peutConstruire, int cashManquant, int propManquante, Boolean resteMaisons, Boolean resteHotels, Boolean hasHotels) {
        if (peutConstruire) {
            System.out.println("\n(Pour comprendre comment intéragir avec la fonction 'Construire', regardez sa section dans les règles !)");
            System.out.println("\nVoulez-vous construire ?");
            Scanner sc = new Scanner(System.in);
            boolean OK = false;
            String rep = null;

            do {
                try {
                    rep = sc.nextLine();
                    if (!rep.toLowerCase().equals("oui") && !rep.toLowerCase().equals("non")) {
                        throw new IOException();
                    }
                    OK = true;

                } catch (IOException ioe) {
                    System.out.println("La réponse doit être oui ou non! Recommencez : \t");

                }
            } while (OK == false);
            return rep.toLowerCase().equals("oui");

        } else {

            System.out.print("Impossible de constuire ici (");

            if (cashManquant != 0) {
                System.out.print("Il vous manque " + cashManquant + "€,");
            }

            if (propManquante != 0) {
                System.out.print(" Il vous manque " + propManquante + " propriété(s),");
            }

            if (resteMaisons == false) {
                System.out.print(" Il ne reste plus de maisons disponibles,");
            }

            if (resteHotels == false) {
                System.out.print(" Il ne reste plus d'hotels disponibles,");
            }

            if (hasHotels == true) {
                System.out.print(" toutes les proprietes de ce groupe ont un hotel,");
            }
            System.out.println(") ");
            return false;
        }

    }

    public Boolean demandeEncoreConstruction(Boolean peutConstruire, int cashManquant, int propManquante, Boolean resteMaisons, Boolean resteHotels, Boolean hasHotels) {
        if (peutConstruire) {
            System.out.println("Appuyez sur entrée pour continuer à construire ou tapez non pour arrêter.");
            Scanner sc = new Scanner(System.in);
            boolean OK = false;
            String rep = null;

            do {
                try {
                    rep = sc.nextLine();
                    if (!rep.toLowerCase().equals("") && !rep.toLowerCase().equals("non")) {
                        throw new IOException();
                    }
                    OK = true;

                } catch (IOException ioe) {
                    System.out.println("Entrez une réponse valide Recommencez : \t");

                }
            } while (OK == false);
            return rep.toLowerCase().equals("");

        } else {
            if (hasHotels == true){
                System.out.println("\nVous en avez fini avec ce groupe !\n");
            } else {
                System.out.print("Impossible de constuire ici (");

            if (cashManquant != 0) {
                System.out.print("Il vous manque " + cashManquant + "€,");
            }

            if (propManquante != 0) {
                System.out.print(" Il vous manque " + propManquante + " propriété(s),");
            }

            if (resteMaisons == false) {
                System.out.print(" Il ne reste plus de maisons disponibles,");
            }

            if (resteHotels == false) {
                System.out.print(" Il ne reste plus d'hotels disponibles,");
            }

            System.out.println(") ");
            }            

            return false;
        }

    }

    /**
     * Demande au joueur sur laquelle de ses propriétés il veut construire.
     * Appelle ensuite le noyau foctionne(méthode construire) pour procéder à la
     * construction.
     *
     * @param pap Liste des propriétés sur lesquelles le joueur peut construire.
     * @see ProprieteAConstruire
     * @see ProprieteAConstruire#construire(Jeu.Joueur, Jeu.Monopoly)
     */
    public void construction(ArrayList<ProprieteAConstruire> pap, Joueur j) {
        boolean OK;
        int i = 0;
        int numPap = 0;
        ProprieteAConstruire ProprieteOuConstruire;
        Scanner sc = new Scanner(System.in);
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Vous avez " + j.getCash() + " € et vous possèdez : ");
        for (Gare g : j.getGares()) {
            System.out.println(g.getNomCarreau());
        }
        for (Compagnie c : j.getCompagnies()) {
            System.out.println(c.getNomCarreau());
        }
        for (ProprieteAConstruire pac : j.getProprietesAConstruire()) {
            System.out.println(pac.getNomCarreau() + " propriété " + pac.getGroupePropriete().getCouleur() + " avec " + pac.getNbMaisons() + " maisons et " + pac.getNbHotels() + " hôtels.");
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        if (!pap.isEmpty()) {
            System.out.println("Sur quelle propriete voulez-vous construire?");
            System.out.println("\n option n°0.\tJe ne veux pas construire finalement.");
            for (ProprieteAConstruire p : pap) {
                i++;
                System.out.println("\n option n°" + i + ".\t" + p.getNomCarreau() + ", située en " + p.getNumero() + " : \t");

            }
            System.out.println("\nEntrez le numéro de l'option :");
            OK = false;
            do {
                try {
                    numPap = sc.nextInt();                          /*Ici on fait une saisie sécurisée du numero de la propriete*/

                    if (numPap < 0 || numPap > (pap.size())) {
                        throw new InputMismatchException();
                    }
                    OK = true;
                } catch (InputMismatchException ime) {
                    System.out.println("Le numero d'une des options doit être un chiffre compris entre 0 et " + (pap.size()) + ".\nRecommencez :\t");
                    OK = false;
                    sc.nextLine();
                }
            } while (OK == false);
            if (numPap > 0) {
                ProprieteOuConstruire = pap.get(numPap - 1);
                ProprieteOuConstruire.construire(ProprieteOuConstruire.getProprietaire(), monopoly);
            }
        }

    }

    /**
     * Affiche le fait qu'un joueur est tombé sur un CarreauArgent et qu'il doit
     * payer.
     *
     * @param nomCase Le nom de la case sur laquelle le joueur est tombé.
     * @param nomJ Le nom du joueur qui est tombé sur la case.
     * @param montant Le montant que le joueur doit payer.
     */
    public void casePayer(String nomCase, String nomJ, int montant) {
        if (!nomCase.contains("Simple Visite / En Prison")) {
            System.out.println("Vous êtes tombé(e) sur " + nomCase + ". Il faut payer " + abs(montant) + "€.");
        }
    }

    /**
     * Affiche le fait qu'un joueur soit tombé sur une propriété d'un autre
     * joueur et qu'il doit lui verser un loyer.
     *
     * @param nomProp Le nom de la propriété sur laquelle le joueur est tombé.
     * @param nomJ Le nom du joueur qui est tombé sur la propriété.
     * @param nomJProprio Le nom du joueur possédant la propriété.
     * @param loyer Le montant que le joueur tombé sur la propriété doit verser
     * au joueur la possédant.
     */
    public void payerLoyer(String nomProp, String nomJ, String nomJProprio, int loyer) {
        System.out.println("Vous êtes tombé(e) sur " + nomProp + ", une propriété de " + nomJProprio + ".");
        System.out.println("Vous versez donc " + loyer + "€ à " + nomJProprio);

    }

    /**
     * Affiche au joueur le type et l'effet d'une carte chance ou communauté.
     *
     * @param t Type de la carte.
     * @param description Efet/Description de la carte.
     * @see Carte
     * @see Monopoly#cartesChance
     * @see Monopoly#cartesCommunaute
     */
    public void afficheCarte(TypeCarte t, String description) {
        System.out.println("Carte " + t.toString() + ": " + description + ".");
    }

    /**
     * Cas où un joueur tombe sur "Allez en prison". Affiche qu'il y va.
     *
     * @see CarreauMouvement
     */
    public void versPrison() {
        System.out.println("Vous allez en prison");
    }

    /**
     * Lors du tour du joueur, si il est en prison, cette méthode demande au
     * joueur si il veut utiliser une carte libéré de prison. Prérequis : Le
     * joueur doit posséder au moins une carte libéré de prison.
     *
     * @return Booléen représentant la volonté du joueur. True = Il veut
     * utiliser une carte libéré de prison. False = Il ne veut pas.
     */
    public boolean UtiliserCarteLibPrison(Joueur j) {
        Scanner sc = new Scanner(System.in);
        System.out.println(j.getNomJoueur() + ", voulez-vous utiliser une carte libéré de prison ?");
        boolean OK = false;
        String rep = null;

        do {
            try {
                rep = sc.nextLine();
                if (!rep.toLowerCase().equals("oui") && !rep.toLowerCase().equals("non")) {
                    throw new IOException();
                }
                OK = true;

            } catch (IOException ioe) {
                System.out.println("La réponse doit être oui ou non! Recommencez : \t");
            }
        } while (!OK);

        if (rep == "oui") {
            System.out.println("Votre carte est remise dans le paquet et vous êtes libre !");
        }

        return rep.equals("oui");
    }

    private void process(ArrayList<? extends CarreauPropriete>... elements) {   /*Test de polymorphisme, pas important pour l'instant*/

        ArrayList<CarreauPropriete> temp = new ArrayList<>();
        for (ArrayList<? extends CarreauPropriete> e : elements) {
            temp.addAll(e);

        }
        for (CarreauPropriete a : temp) {
            a.getNomCarreau();
            System.out.println(a.getClass());
        }
    }

}
